# About
 * Core functionality for multiple sensors (accelerometer, gyroscope,
   magnetometer, barometer, etc.).
 * This module allow to access data from sensors via generalized API.
 * Module automatically determine which sensor is connected and automatically
   setup itself accordingly.
 * For reference, take a look to `sensors.h`. There is most of API you will
   need. Also example can help you.

# Supported HW

* `MPU6050` 
  * Done
  * Part of `GY-521` board
  * Accelerometer, gyroscope
* `HM5883L` 
  * To do
  * Part of `CJMCU-008` board
  * Magnetometer
* `MPU9250`
  * To do
  * Part of `GY-91` board
  * Accelerometer, gyroscope, magnetometer
* `BMP280`
  * To do
  * Part of `GY-91` board
  * Pressure sensor

# How it works

* Sensor logic load all known drivers
* Check which devices are available
* When you need to data from accelerometer, you do not need to worry about
  which sensor is available and how it returns data. You simply call *get*
  function with required feature (accelerometer, gyroscope, magnetometer, etc.)
  and you will get data.
* If required feature is not available, the `SENSOR_NOT_SUPPORTED` error code
  is returned.
