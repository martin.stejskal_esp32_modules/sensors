/**
 * @file
 * @author Martin Stejskal
 * @brief Common routines & macros for sensor logic
 *
 * Mainly used by drivers, but also upper layers can use those functions.
 * This header should be architecture independent, but it may include
 * architecture dependent headers in order to properly define function mapping.
 */
#ifndef __SENSORS_COMMON_H__
#define __SENSORS_COMMON_H__
// ===============================| Includes |================================
#include <stdbool.h>
#include <stdint.h>

// Following allow to load "global" settings from one file
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif

// Based on current platform, load appropriate header file
#if defined(ESP_PLATFORM)
#include "esp.h"
#else
#error "Can not find platform specific header and therefore platform specific"
" functions like sensor_nvs_get(), sensor_read_reg() and sensor_get_time_ms()"
#endif

// ================================| Defines |================================
/**
 * @brief Macro which simplify handling error codes
 */
#define SENSORS_RET_IF_ERR(errCodeFromFunction)      \
  do {                                               \
    e_sensor_error __err_rc = (errCodeFromFunction); \
    if (__err_rc != 0) {                             \
      return (__err_rc);                             \
    }                                                \
  } while (0)

/**
 * @brief Helps to figure out number of items in the array
 */
#define SENSORS_NUM_OF_ITEMS(array) (sizeof(array) / sizeof(array[0]))
// ============================| Default values |=============================

/**
 * @brief Log related macros
 *
 * In case that architecture specific file will not define log functions,
 * defaults below can be used as kind of fallback. However, it is highly
 * recommended to define custom function per architecture.
 *
 * @note Here is beneficial to use macro, instead of function, because in
 * case of function, additional memory buffer would have to be defined
 */
#ifndef SENSORS_LOGE
#define SENSORS_LOGE(tag, ...) \
  printf("E %s : ", tag);      \
  printf(__VA_ARGS__);         \
  printf("\n")
#endif

#ifndef SENSORS_LOGW
#define SENSORS_LOGW(tag, ...) \
  printf("W %s : ", tag);      \
  printf(__VA_ARGS__);         \
  printf("\n")
#endif

#ifndef SENSORS_LOGI
#define SENSORS_LOGI(tag, ...) \
  printf("I %s : ", tag);      \
  printf(__VA_ARGS__);         \
  printf("\n")
#endif

#ifndef SENSORS_LOGD
#define SENSORS_LOGD(tag, ...) \
  printf("D %s : ", tag);      \
  printf(__VA_ARGS__);         \
  printf("\n")
#endif

#ifndef SENSORS_LOGV
#define SENSORS_LOGV(tag, ...) \
  printf("V %s : ", tag);      \
  printf(__VA_ARGS__);         \
  printf("\n")
#endif

// ===========================| Structures, enums |===========================
// List of error codes used across sensors modules
typedef enum {
  ///@brief No error, everything cool
  SENSOR_OK = 0,
  SENSOR_FAIL,
  SENSOR_NOT_SUPPORTED,
  SENSOR_NOT_INITIALIZED,
  SENSOR_DEVICE_NOT_FOUND,
  SENSOR_INVALID_PARAM,
  SENSOR_INVALID_DATA_TYPE,
  SENSOR_PERIPHERAL_INIT_FAIL,
  SENSOR_PERIPHERAL_DEINIT_FAIL,
  SENSOR_INVALID_STATE,
  SENSOR_ALREADY_RUNNING,
  SENSOR_NOT_RUNNING,
  SENSOR_TIMER_INITIALIZATION_FAILED,
  SENSOR_TIMER_DEINITIALIZATION_FAILED,
  SENSOR_EMPTY_POINTER,
  SENSOR_INTERNAL_ERROR,
  SENSOR_TIMEOUT,
  SENSOR_INVALID_VALUE,

  // When sensor record some out of range value and can not perform operation
  SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS,

} e_sensor_error;

/**
 * @brief List of overall features
 */
typedef enum {
  SENSOR_FEATURE_ACCELEROMETER = 0,
  SENSOR_FEATURE_GYROSCOPE,
  SENSOR_FEATURE_MAGENETOMETER,
  SENSOR_FEATURE_TEMPERATURE,
  SENSOR_FEATURE_PRESSURE,

  SENSOR_FEATURE_MAX  // Last item
} te_sensor_feature;

/**
 * @brief Calibration levels
 */
typedef enum {
  ///@brief Very fast calibration, but quite inaccurate
  SENSOR_CALIB_SUPER_FAST,
  SENSOR_CALIB_FAST,

  ///@brief Good balance between speed and accuracy
  SENSOR_CALIB_NORMAL,
  SENSOR_CALIB_SLOW,

  ///@brief You want to make accurate measurement
  SENSOR_CALIB_SUPER_SLOW,

  ///@brief Just waste of time to make user think that device is hard working
  SENSOR_CALIB_SLOTH,
  SENSOR_CALIB_MAX,
} ts_sensor_calibration;

/**
 * @brief Simplify dealing with 16bit data
 *
 * Sensors are typically use 16bit data, so this simple trick can quite help
 */
typedef union {
  struct {
    // Little endian is expected
    uint8_t u8_low;
    uint8_t u8_high;
  } s;
  uint16_t u16;
} tu_sensor_16_bit;

/**
 * @brief Describe 16bit 3D vector
 */
typedef struct {
  int16_t i16_x;
  int16_t i16_y;
  int16_t i16_z;
} ts_sensor_vect;

/**
 * @brief Vector for float values
 */
typedef struct {
  float f_x;
  float f_y;
  float f_z;
} ts_sensor_vect_float;

/**
 * @brief Universal structure for all kind of data
 */
typedef union {
  // Expected to be used in case of accelerometer, gyroscope or magnetometer
  ts_sensor_vect s_vect;

  // Expected for temperature or pressure when reading raw values
  int16_t i16;

  // Used for accelerometer, gyroscope or magnetometer but when float is
  // required
  ts_sensor_vect_float s_vect_float;

  // Used for temperature or pressure when float is required
  float f;
} tu_sensor_data_content;

/**
 * @brief Data types for description of data content
 *
 * @ref tuSensorDataContent
 */
typedef enum {
  SENSOR_DTYPE_RAW,       //!< SENSOR_DTPYE_RAW Raw integer value
  SENSOR_DTYPE_RAW_VECT,  //!< SENSOR_DTYPE_RAW_VECT Raw vector integer

  SENSOR_DTYPE_INT,       //!< SENSOR_DTYPE_INT Integer value
  SENSOR_DTYPE_INT_VECT,  //!< SENSOR_DTYPE_INT_VECT Vector integer

  SENSOR_DTYPE_FLOAT,      //!< SENSOR_DTYPE_FLOAT Float value
  SENSOR_DTYPE_FLOAT_VECT  //!< SENSOR_DTYPE_FLOAT_VECT Vector float
} te_sensor_data_type;

/**
 * @brief Structure used to pass data from sensor
 *
 * Type reports used data type in which are data passed. This can be used for
 * double check or it can help when processing on some high abstraction layer
 */
typedef struct {
  te_sensor_data_type e_data_type;
  te_sensor_feature e_feature;

  tu_sensor_data_content u_d;
} ts_sensor_utils_data;

/**
 * @brief Define callback types
 *
 * @{
 */

/**
 * @brief Callback for initialization and de-initialization
 *
 * @return Error code. SENSOR_OK if no error.
 */
typedef e_sensor_error (*tf_sensor_init_cb)(void);

/**
 * @brief Callback for reading raw data from sensor
 * @param e_feature Item from feature list
 * @param[out] puData Pointer to memory where data will be stored
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
typedef e_sensor_error (*tf_sensor_get_raw_cb)(
    const te_sensor_feature e_feature, ts_sensor_utils_data *ps_data);

/**
 * @brief Callback for converting raw value to integer
 * @param s_in_raw Input structure from "get raw" function
 * @param[out] ps_out Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
typedef e_sensor_error (*tf_sensor_conv_to_int_cb)(
    const ts_sensor_utils_data s_in_raw, ts_sensor_utils_data *ps_out);

typedef e_sensor_error (*tf_sensor_conv_to_float_cb)(
    const ts_sensor_utils_data s_in_raw, ts_sensor_utils_data *ps_out);

typedef e_sensor_error (*tf_sensor_calibrate_cb)(
    const te_sensor_feature e_feature, const ts_sensor_calibration e_level);

typedef e_sensor_error (*tf_sensor_need_calibrate_cb)(
    const te_sensor_feature e_feature, bool *pb_need_calibrate);
/**
 * @}
 */

typedef struct {
  // Pointer to sensor name. Typically it is aligned with file name
  const char *pac_name;

  // Every bit represent feature. Refer to @ref teSensorFeature
  uint32_t u32_features;

  struct {
    uint32_t u32_min_speed_bps;
    uint32_t u32_max_speed_bps;
    uint32_t u32_recommended_speed_bps;
  } i2c;

  // List of callbacks
  struct {
    tf_sensor_init_cb pf_init;
    tf_sensor_init_cb pf_deinit;

    tf_sensor_get_raw_cb pf_get_raw;

    // Physical value
    //  * accelerometer - mg
    //  * gyroscope - degrees per second
    //  * magnetometer - uT
    //  * temperature - 10x degree of Celsius
    //  * pressure - 10x Pa
    tf_sensor_conv_to_int_cb pf_conv_to_int;
    tf_sensor_conv_to_float_cb pf_conv_to_float;

    tf_sensor_need_calibrate_cb pf_need_calibration;
    tf_sensor_calibrate_cb pf_calibrate;
  } cb;
} ts_sensor_descriptor;

// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Swap byte order in 16 bit variable
 * @param[in, out] pu_variable Pointed to data
 */
void sensor_swap_bytes_16_bit(tu_sensor_16_bit *pu_variable);

/**
 * @brief Check if device is on bus or not
 *
 * Can be used as simple bus sniffer
 *
 * @param u8_dev_addr I2C address of device
 * @param b_write_mode If 1, then check device availability in "write" mode.
 *        If 0, then check device in "read" mode. Some devices might be
 *        write only for example, so they can return NAC in "read" mode.
 * @return True is device is found on the bus, false otherwise
 */
bool sensor_is_device_on_bus(const uint8_t u8_dev_addr, bool b_write_mode);

/**
 * @brief Returns system time in milliseconds
 * @return Time in milliseconds
 */
uint32_t sensor_get_time_ms(void);

/**
 * @brief Take vector, multiply it and then divide
 *
 * Due to multiplication the overflow could happen, so this function take care
 * about it. This function is useful when recalculating raw value to
 * "real integer".
 *
 * @param ps_vect Source vector
 * @param i16_multiplier Multiplier
 * @param i16_divider Divider
 */
void sensor_multiply_and_divide_int_vector(ts_sensor_vect *ps_vect,
                                           int16_t i16_multiplier,
                                           int16_t i16_divider);

/**
 * @brief Get recommended number of iterations for calibration process
 *
 * Different sensors use same way how to calibrate it's features. And in order
 * to calibrate it properly, typically N number of iteration cycles is needed.
 * This value might differ sensor by sensor, but not necessary. So this
 * function can be used by driver as reference value. Of course, it is up to
 * driver itself if this function will use or if it will define own thresholds.
 *
 * @param e_feature Selected sensor feature
 * @param e_level Calibration level in human friendly form
 *
 * @return Absolute number of iterations needed for calibration
 */
uint16_t sensor_get_recommended_num_of_iterations(
    const te_sensor_feature e_feature, const ts_sensor_calibration e_level);

/**
 * @brief Blocking delay in milliseconds
 * @param u32_delay_ms Delay in milliseconds
 *
 * @note Values under 20 ms might not be accurate
 */
void sensor_delay_ms(const uint32_t u32_delay_ms);
// =============================| Print related |=============================
/**
 * @brief Convert data in binary form into string
 *
 * In case of "raw" values, units will not be appended. Otherwise appropriate
 * units will be added.
 *
 * @param ps_data Pointer to data structure
 * @return Pointer to string
 */
const char *sensor_data_to_str(const ts_sensor_utils_data *ps_data);

/**
 * @brief Convert input error code to string interpretation
 * @param e_err_code Error code as enumeration value
 * @return Pointer to string
 */
const char *sensor_error_code_to_str(const e_sensor_error e_err_code);

/**
 * @brief Convert feature enumeration into text
 * @param e_feature One of value from enumeration
 * @return Pointer to text
 */
const char *sensor_feature_to_str(const te_sensor_feature e_feature);

/**
 * @brief Convert input data type to string interpretation
 * @param e_data_type Data type as enumeration
 * @return Pointer to string
 */
const char *sensor_data_type_to_str(const te_sensor_data_type e_data_type);

/**
 * @brief Get units for current data as string
 *
 * @param ps_data Pointer to source data structure
 * @return Pointer to output string
 */
const char *sensor_get_unit_str(const ts_sensor_utils_data *ps_data);

#endif  // __SENSORS_COMMON_H__
