/**
 * @file
 * @author Martin Stejskal
 * @brief Share internal functionality and defines
 *
 * This header should NOT be used by upper layers. Only for internal usage of
 * drivers.
 */
#ifndef __SENSORS_COMMON_PRIVATE_H__
#define __SENSORS_COMMON_PRIVATE_H__
// ===============================| Includes |================================
#include "sensors_common.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Check if device have required value at specific register
 *
 * Reading out "who am I" register is common method to figure out that not only
 * I2C address is correct, but also check if specific device is connected.
 * Typically different I2C devices can use same I2C address. So only way how
 * to distinguish between them is to read out specific "who am I" register
 * and compare value with expected value.
 *
 * @param u8_dev_addr Device address
 * @param u8_reg_addr Register address
 * @param u8_expected_value Expected value in register
 * @return SENSOR_OK if register value match with expected value. Returns
 *         SENSOR_DEVICE_NOT_FOUND if device can not be even found on bus.
 *         Returns SENSOR_INVALID_VALUE if device was found, but required
 *         register value did not match
 */
e_sensor_error sensor_check_who_am_i(const uint8_t u8_dev_addr,
                                     const uint8_t u8_reg_addr,
                                     const uint8_t u8_expected_value);

/**
 * @brief Check if device have at least 1 required value at specific register
 *
 * This function also counts with iteration over bus address.
 *
 * @param pu8_dev_addr_list Pointer to list of device addresses which will be
 *        probed.
 * @param u8_addr_list_num_of_items Number of items in address list.
 * @param u8_reg_addr Register address
 * @param pu8_expected_value_list List of expected values in the register. If
 *        at least one matches, SENSOR_OK is returned.
 * @param u8_value_list_num_of_items Define number of items in expected value
 *        list
 * @param pu8_pu8_found_dev_addr When expected value is found, into this
 *        pointer will be written current device bus address
 * @param pu8_found_value When expected value is found, it will be written
 *        here. Upper layer can use it for another processing if needed.
 *        Optional parameter. If empty, nothing will be written here.
 *
 * @return SENSOR_EMPTY_POINTER is empty pointer is given. SENSOR_INVALID_PARAM
 *         if number of items of any list is zero. SENSOR_OK if at least at
 *         one bus address matching at least expected value.
 *         SENSOR_DEVICE_NOT_FOUND otherwise.
 */
e_sensor_error sensor_check_who_am_i_list(
    const uint8_t *pu8_dev_addr_list, const uint8_t u8_addr_list_num_of_items,
    const uint8_t u8_reg_addr, const uint8_t *pu8_expected_value_list,
    const uint8_t u8_value_list_num_of_items, uint8_t *pu8_found_dev_addr,
    uint8_t *pu8_found_value);

// ========================| Middle level functions |=========================
/**
 * @brief Load setting from NVS
 *
 * @note Platform specific function
 *
 * @note This function is used internally by "drivers". Should not be used by
 *       application layer
 *
 * @param pac_key Unique key identificator as string
 * @param p_out_value Pointer to memory where loaded data will be written
 * @param p_default_value Pointer to default data. Default data will be used
 *                        in case that key is not registered yet, so at least
 *                        expected data will be populated to output variable.
 * @param i_size Size of structure/array which handle data. Size is in Bytes
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_nvs_get(const char *pac_key, void *p_out_value,
                              const void *p_default_value, size_t i_size);

/**
 * @brief Set settings to NVS
 *
 * @note Platform specific function
 *
 * @note This function is used internally by "drivers". Should not be used by
 *       application layer
 *
 * @param pac_key Unique key identificator as string
 * @param p_in_value Pointer to memory from where data will be loaded
 * @param i_size Size of data to be stored
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_nvs_set(const char *pac_key, const void *p_in_value,
                              size_t i_size);

/**
 * @brief Delete settings from NVS
 *
 * @note Platform specific function
 *
 * @note This function is used internally by "drivers". Should not be used by
 *       application layer
 *
 * @param pac_key Unique key identificator as string
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_nvs_del(const char *pac_key);

/**
 * @brief Calibration process for gyroscope
 *
 * @note This function is used internally by "drivers". Should not be used by
 *       application layer
 *
 * @param pf_get_raw Pointer to function which gives raw data from sensor
 * @param u16_num_of_iterations Number of iterations for calibration process
 * @param[out] ps_correction Correction vector will be written here
 *
 * @return SENSOR_OK of no error
 */
e_sensor_error sensor_calibrate_gyroscope(const tf_sensor_get_raw_cb pf_get_raw,
                                          const uint16_t u16_num_of_iterations,
                                          ts_sensor_vect *ps_correction);

/**
 * @brief Calibration process for magnetometer
 *
 * @param pf_get_raw Pointer to function which gives raw data from sensor
 * @param u16_num_of_iterations Number of iterations for calibration process
 * @param[out] ps_offset Output offset vector
 * @param[out] ps_scale Scale correction vector
 * @return
 */
e_sensor_error sensor_calibrate_magnetometer(
    const tf_sensor_get_raw_cb pf_get_raw, const uint16_t u16_num_of_iterations,
    ts_sensor_vect *ps_offset, ts_sensor_vect *ps_scale);

// ==========================| Low level functions |==========================
/**
 * @brief Initialize I2C peripheral
 *
 * @note This function is used internally by "drivers". Should not be used by
 *       application layer
 *
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_init_i2c(void);

/**
 * @brief Deinitialize I2C peripheral
 *
 * @note This function is used internally by "drivers". Should not be used by
 *       application layer
 *
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_deinit_i2c(void);

/**
 * @brief Read data from register(s)
 *
 * @param u8_dev_addr Device address
 * @param u8_reg_addr Start register address
 * @param pau8_buffer Pointer to buffer, where read data will be written
 * @param u8_num_of_bytes Number of Bytes to read
 *
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_read_reg(const uint8_t u8_dev_addr,
                               const uint8_t u8_reg_addr, uint8_t *pau8_buffer,
                               const uint8_t u8_num_of_bytes);

/**
 * @brief Write one Byte to register address
 *
 * @param u8_dev_addr Device address
 * @param u8_reg_addr Register address
 * @param u8_value Value which will be written into given register
 *
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_write_reg(const uint8_t u8_dev_addr,
                                const uint8_t u8_reg_addr,
                                const uint8_t u8_value);

// =============================| Print related |=============================
/**
 * @brief Internal version of @ref sensor_scan_bus_print_found_devices
 *
 * This function does not check, whether main sensor logic is initialized or
 * not. Avoiding this check allow help developers when working on drivers,
 * but this function should not be ever used as public API for user.
 *
 * @return SENSOR_OK if no error
 */
e_sensor_error sensor_scan_bus_print_found_devices_priv(void);
#endif  // __SENSORS_COMMON_PRIVATE_H__
