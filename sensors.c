/**
 * @file
 * @author Martin Stejskal
 * @brief Service sensors
 */
// ===============================| Includes |================================
#include "sensors.h"

#include <assert.h>
#include <string.h>

#include "hscdtd008a.h"
#include "mpu6050.h"
#include "mpu9250.h"
#include "sensors_common_private.h"

// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief List of supported sensors as enumeration
 *
 * This enumeration is used also as index for reaching descriptors
 */
typedef enum {
  MPU6050 = 0,
  MPU9250,
  HSCDTD008A,
  SENSOR_LIST_MAX
} te_sensor_list;

typedef struct {
  // List of all sensors and their descriptors
  const ts_sensor_descriptor *pas_sensor_descriptor[SENSOR_LIST_MAX];

  // List of all features and their descriptors (kind of pointer to used
  // driver)
  const ts_sensor_descriptor *pas_feature_descriptor[SENSOR_FEATURE_MAX];

  // Keep info if sensors logic was initialized or not
  bool b_initialized;
} ts_sensors_runtime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *tag = "Sensors";

static ts_sensors_runtime ms_runtime;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Get descriptors from all sensors
 */
static void _load_sensor_descriptors(void);

/**
 * @brief Try to initialize sensors and assign feature appropriate descriptor
 *
 * Idea is that every feature is basically just pointer to driver (sensor
 * descriptor). This function check what is available and link feature with
 * driver (if available).
 */
static void _load_feature_descriptors(void);

static e_sensor_error _check_feature_and_type(const te_sensor_feature e_feature,
                                              const te_sensor_data_type e_type);

/**
 * @brief Convert sensor type to human readable string
 * @param eSensorType Sensor type. Select one of the value from enumeration
 * @return Pointer to string
 */
static const char *_sensor_type_to_str(const te_sensor_list e_sensor_type);

// ============================| Dummy functions |============================
/**
 * @brief Dummy functions which only returns "not supported" error code
 *
 * In order to avoid additional checking in code, if driver for some function
 * is not available, those dummy functions will be used instead. This can make
 * code easier to write and read, since those dummy functions basically cover
 * checking inputs basically.
 *
 * @{
 */
e_sensor_error _dummy_init(void);
e_sensor_error _dummy_deinit(void);
e_sensor_error _dummy_get_raw(const te_sensor_feature e_feature,
                              ts_sensor_utils_data *ps_data);
e_sensor_error _dummy_convert_to_int(const ts_sensor_utils_data s_in_raw,
                                     ts_sensor_utils_data *ps_out);
e_sensor_error _dummy_convert_to_float(const ts_sensor_utils_data s_in_raw,
                                       ts_sensor_utils_data *ps_out);
e_sensor_error _dummy_need_calibrate(const te_sensor_feature e_feature,
                                     bool *pb_need_calibrate);
e_sensor_error _dummy_calibrate(const te_sensor_feature e_feature,
                                const ts_sensor_calibration e_level);
/**
 * @}
 * @param pvParameters
 */
// =========================| High level functions |==========================
e_sensor_error sensors_initialize(void) {
  if (ms_runtime.b_initialized) {
    // Already initialized. Nothing to do
    return SENSOR_OK;
  }

  // This have to be second thing. If something goes wrong, it shall be
  // assumed, that sensors are not initialized
  ms_runtime.b_initialized = false;

  SENSORS_LOGD(tag, "Probing available sensors");

  e_sensor_error e_err_code = SENSOR_FAIL;

  // First have to be loaded sensor descriptors, so we know with what we can
  // count
  _load_sensor_descriptors();

  // Now it is good idea to initialize I2C. We will need functional bus to
  // figure out which sensors are available
  e_err_code = sensor_init_i2c();
  if (e_err_code) {
    return e_err_code;
  }

  // Figure out which features will be available
  _load_feature_descriptors();

  SENSORS_LOGD(tag, "Initialization done");

  // Now we can proudly say, that sensors are initialized
  assert(e_err_code == SENSOR_OK);
  ms_runtime.b_initialized = true;

  return e_err_code;
}

e_sensor_error sensors_deinitialize(void) {
  e_sensor_error e_err_code = SENSOR_OK;

  if (!ms_runtime.b_initialized) {
    // Actually not initialized yet -> return OK
    return e_err_code;
  }

  // From this point, no matter what happens, we must assume that sensors are
  // not initialized anymore
  ms_runtime.b_initialized = false;

  // Temporary holder for captured error code when deinitializing
  e_sensor_error e_err_code_deinit;

  for (te_sensor_feature e_feature = (te_sensor_feature)0;
       e_feature < SENSOR_FEATURE_MAX; e_feature++) {
    // Try deinitialize feature, if not pointing to dummy function (feature is
    // not available)
    tf_sensor_init_cb pf_deinit =
        ms_runtime.pas_feature_descriptor[e_feature]->cb.pf_deinit;

    if (pf_deinit != _dummy_deinit) {
      e_err_code_deinit = pf_deinit();

      if (e_err_code_deinit) {
        // Some error detected - just record errors. At the end just evaluate
        // if everything went smooth or not. There is not much to do anyway.
        e_err_code = e_err_code_deinit;
      }
    }
  }

  // As last step, I2C peripheral can be deinitialized, since no longer is
  // required to communicate with sensors
  e_err_code_deinit = sensor_deinit_i2c();

  // If final deinitialization passed, simply return error code from modules
  if (e_err_code_deinit == SENSOR_OK) {
    // Return error code from module
    return e_err_code;

  } else {
    // Return error code from I2C
    return e_err_code_deinit;
  }
}

bool sensors_are_initialized(void) { return ms_runtime.b_initialized; }

bool sensor_is_available(const te_sensor_feature e_feature) {
  // Value should not exceed MAX
  assert(e_feature < SENSOR_FEATURE_MAX);

  // If feature is not available, dummy functions are used instead. So
  // technically is OK to compare pointer to initialization function with
  // dummy function. If they match, it means feature is basically not available
  if (ms_runtime.pas_feature_descriptor[e_feature]->cb.pf_init == _dummy_init) {
    return false;
  } else {
    return true;
  }
}

e_sensor_error sensor_get(const te_sensor_feature e_feature,
                          ts_sensor_utils_data *ps_data) {
  e_sensor_error e_err_code =
      _check_feature_and_type(e_feature, ps_data->e_data_type);
  if (e_err_code) {
    return e_err_code;
  }

  e_err_code = SENSOR_FAIL;

  // Assume that feature & data type is OK
  if ((ps_data->e_data_type == SENSOR_DTYPE_RAW) ||
      (ps_data->e_data_type == SENSOR_DTYPE_RAW_VECT)) {
    // Get raw
    e_err_code = sensor_get_raw(e_feature, ps_data);

  } else if ((ps_data->e_data_type == SENSOR_DTYPE_INT) ||
             (ps_data->e_data_type == SENSOR_DTYPE_INT_VECT)) {
    // Get normalized integer
    e_err_code = sensor_get_int(e_feature, ps_data);
  } else if ((ps_data->e_data_type == SENSOR_DTYPE_FLOAT) ||
             (ps_data->e_data_type == SENSOR_DTYPE_FLOAT_VECT)) {
    // Get float value
    e_err_code = sensor_get_float(e_feature, ps_data);

  } else {
    // unknown data type - actually not expected
  }

  return e_err_code;
}
// ========================| Middle level functions |=========================
e_sensor_error sensor_get_int(const te_sensor_feature e_feature,
                              ts_sensor_utils_data *ps_data) {
  ///@todo Deal with I2C & options
  // First load value, then convert it
  const ts_sensor_descriptor *ps_descriptor =
      ms_runtime.pas_feature_descriptor[e_feature];

  ts_sensor_utils_data s_raw_data;

  SENSORS_RET_IF_ERR(ps_descriptor->cb.pf_get_raw(e_feature, &s_raw_data));

  return ps_descriptor->cb.pf_conv_to_int(s_raw_data, ps_data);
}

e_sensor_error sensor_get_float(const te_sensor_feature e_feature,
                                ts_sensor_utils_data *ps_data_float) {
  ///@todo Deal with I2C & options
  // First load value, then convert it
  const ts_sensor_descriptor *ps_descriptor =
      ms_runtime.pas_feature_descriptor[e_feature];

  ts_sensor_utils_data s_raw_data;

  SENSORS_RET_IF_ERR(ps_descriptor->cb.pf_get_raw(e_feature, &s_raw_data));

  return ps_descriptor->cb.pf_conv_to_float(s_raw_data, ps_data_float);
}

e_sensor_error sensor_need_calibrate(const te_sensor_feature e_feature,
                                     bool *pb_need_calibrate) {
  ///@todo Deal with I2C & options
  const ts_sensor_descriptor *ps_descriptor =
      ms_runtime.pas_feature_descriptor[e_feature];

  return ps_descriptor->cb.pf_need_calibration(e_feature, pb_need_calibrate);
}

e_sensor_error sensor_calibrate(const te_sensor_feature e_feature,
                                const ts_sensor_calibration e_level) {
  ///@todo Deal with I2C & options
  const ts_sensor_descriptor *ps_descriptor =
      ms_runtime.pas_feature_descriptor[e_feature];

  return ps_descriptor->cb.pf_calibrate(e_feature, e_level);
}
// ==========================| Low level functions |==========================
e_sensor_error sensor_get_raw(const te_sensor_feature e_feature,
                              ts_sensor_utils_data *ps_data) {
  ///@todo Deal with I2C & options
  return ms_runtime.pas_feature_descriptor[e_feature]->cb.pf_get_raw(e_feature,
                                                                     ps_data);
}

// =============================| Print related |=============================
e_sensor_error sensor_scan_bus_print_found_devices(void) {
  // If not initialized, then can not probe I2C bus
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  return sensor_scan_bus_print_found_devices_priv();
}

// ==========================| Internal functions |===========================

static void _load_sensor_descriptors(void) {
  ms_runtime.pas_sensor_descriptor[MPU6050] = mpu6050_get_descriptor();
  ms_runtime.pas_sensor_descriptor[MPU9250] = mpu9250_get_descriptor();
  ms_runtime.pas_sensor_descriptor[HSCDTD008A] = hscdtd008a_get_descriptor();
}

static void _load_feature_descriptors(void) {
  e_sensor_error e_err_code = SENSOR_FAIL;

  // Dummy descriptor which might be used if all features are not available
  static const ts_sensor_descriptor s_dummy_descriptor = {
      .cb.pf_init = _dummy_init,
      .cb.pf_deinit = _dummy_deinit,
      .cb.pf_get_raw = _dummy_get_raw,
      .cb.pf_conv_to_int = _dummy_convert_to_int,
      .cb.pf_conv_to_float = _dummy_convert_to_float,
      .cb.pf_need_calibration = _dummy_need_calibrate,
      .cb.pf_calibrate = _dummy_calibrate,
  };

  // By default, set all feature descriptors callbacks to dummy functions
  for (te_sensor_feature e_feature = (te_sensor_feature)0;
       e_feature < SENSOR_FEATURE_MAX; e_feature++) {
    ms_runtime.pas_feature_descriptor[e_feature] = &s_dummy_descriptor;
  }

  // Try to initialize driver and if succeeds, assign feature descriptors
  for (te_sensor_list e_sensor_type = (te_sensor_list)0;
       e_sensor_type < SENSOR_LIST_MAX; e_sensor_type++) {
    // Just make it easier to type - get pointer to current sensor descriptor
    const ts_sensor_descriptor *p_sensor_descriptor =
        ms_runtime.pas_sensor_descriptor[e_sensor_type];

    e_err_code = p_sensor_descriptor->cb.pf_init();

    if (e_err_code) {
      // Initialization failed - report it
      SENSORS_LOGI(tag, "Failed to initialize \"%s\" - reason: %s",
                   _sensor_type_to_str(e_sensor_type),
                   sensor_error_code_to_str(e_err_code));
    } else {
      SENSORS_LOGI(tag, "Sensor \"%s\" initialized (^_^)",
                   _sensor_type_to_str(e_sensor_type));

      // Register supported features
      for (te_sensor_feature e_feature = (te_sensor_feature)0;
           e_feature < SENSOR_FEATURE_MAX; e_feature++) {
        // If current feature is supported, register pointer to driver (sensor
        // descriptor)
        if (p_sensor_descriptor->u32_features & (1 << e_feature)) {
          ms_runtime.pas_feature_descriptor[e_feature] = p_sensor_descriptor;
        }
      }
    }
  }
}

static e_sensor_error _check_feature_and_type(
    const te_sensor_feature e_feature, const te_sensor_data_type e_type) {
  e_sensor_error e_err_code = SENSOR_FAIL;

  if ((e_feature == SENSOR_FEATURE_ACCELEROMETER) ||
      (e_feature == SENSOR_FEATURE_GYROSCOPE) ||
      (e_feature == SENSOR_FEATURE_MAGENETOMETER)) {
    // Vector features
    if ((e_type == SENSOR_DTYPE_RAW_VECT) ||
        (e_type == SENSOR_DTYPE_INT_VECT) ||
        (e_type == SENSOR_DTYPE_FLOAT_VECT)) {
      e_err_code = SENSOR_OK;
    } else {
      e_err_code = SENSOR_INVALID_DATA_TYPE;
    }
  } else {
    // One value
    if ((e_type == SENSOR_DTYPE_RAW) || (e_type == SENSOR_DTYPE_INT) ||
        (e_type == SENSOR_DTYPE_FLOAT)) {
      e_err_code = SENSOR_OK;
    } else {
      e_err_code = SENSOR_INVALID_DATA_TYPE;
    }
  }

  return e_err_code;
}

static const char *_sensor_type_to_str(const te_sensor_list e_sensor_type) {
  assert(e_sensor_type < SENSOR_LIST_MAX);

  return ms_runtime.pas_sensor_descriptor[e_sensor_type]->pac_name;
}

// ============================| Dummy functions |============================
e_sensor_error _dummy_init(void) { return SENSOR_NOT_SUPPORTED; }

e_sensor_error _dummy_deinit(void) { return SENSOR_NOT_SUPPORTED; }

e_sensor_error _dummy_get_raw(const te_sensor_feature e_feature,
                              ts_sensor_utils_data *ps_data) {
  return SENSOR_NOT_SUPPORTED;
}

e_sensor_error _dummy_convert_to_int(const ts_sensor_utils_data s_in_raw,
                                     ts_sensor_utils_data *ps_out) {
  return SENSOR_NOT_SUPPORTED;
}

e_sensor_error _dummy_convert_to_float(const ts_sensor_utils_data s_in_raw,
                                       ts_sensor_utils_data *ps_out) {
  return SENSOR_NOT_SUPPORTED;
}

e_sensor_error _dummy_need_calibrate(const te_sensor_feature e_feature,
                                     bool *pb_need_calibrate) {
  return SENSOR_NOT_SUPPORTED;
}

e_sensor_error _dummy_calibrate(const te_sensor_feature e_feature,
                                const ts_sensor_calibration e_level) {
  return SENSOR_NOT_SUPPORTED;
}
