/**
 * @file
 * @author Martin Stejskal
 * @brief HAL for MPU6050 chip
 */
// ===============================| Includes |================================
#include "mpu6050.h"

#include <assert.h>
#include <string.h>

#include "sensors_common_private.h"
// ================================| Defines |================================
/**
 * @brief Register numbers
 *
 * @{
 */
#define MPU6050_REG_ACCEL_XOUT_H (59)
#define MPU6050_REG_TEMP_OUT_H (65)
#define MPU5050_REG_GYRO_XOUT_H (67)
#define MPU6050_REG_SIGNAL_PATH_RESET (104)
#define MPU6050_REG_POWER_MANAMEMENT_1 (107)
#define MPU6050_REG_WHO_AM_I (117)
/**
 * @}
 */

/**
 * @brief Default value for register 107 (Power Management 1)
 */
#define MPU6050_REG_POWER_MANAGEMENT_1_107_DEFAULT (0x40)

/**
 * @brief Default value for register 117 (Who I am)
 */
#define MPU6050_REG_WHO_I_AM_117_DEFAULT (0x68)

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
// ===============================| Registers |===============================
// Expected Little Endian - LSB is "first" in list
typedef union {
  struct {
    uint8_t TEMP_RESET : 1;
    uint8_t ACCEL_RESET : 1;
    uint8_t GYRO_RESET : 1;
    uint8_t : 5;
  } s;
  uint8_t u8_raw;
} tu_mpu6050_sig_path_rst_104;

typedef union {
  struct {
    uint8_t CLKSEL : 3;
    uint8_t TEMP_DIS : 1;
    uint8_t : 1;
    uint8_t CYCLE : 1;
    uint8_t SLEEP : 1;
    uint8_t DEVICE_RESET : 1;
  } s;
  uint8_t u8_raw;
} tu_mpu6050_power_mgmnt_107;

typedef enum {
  R107_CLKSEL_INTERNAL_8MHZ_OSCILLATOR = 0,
  R107_CLKSEL_PLL_WITH_X_AXIS_GYRO_REF = 1,
  R107_CLKSEL_PLL_WITH_Y_AXIS_GYRO_REF = 2,
  R107_CLKSEL_PLL_WITH_Z_AXIS_GYRO_REF = 3,
  R107_CLKSEL_EXT_32768_HZ_REF = 4,
  R107_CLKSEL_EXT_19200_KHZ_REF = 5,
  R107_CLKSEL_RESERVED = 6,
  R107_CLKSEL_STOP_CLK_KEEP_TIMING_GEN_IN_RESET = 7
} te_mpu6050_power_mgmnt_107_clksel;

typedef struct {
  // Is module initialized?
  bool b_initialized;

  // Device address on I2C bus
  uint8_t u8_address;

  // Calibration data for gyroscope
  ts_sensor_vect s_gyro_calib;
} ts_mpu6050_runtime;
// ===========================| Global variables |============================
/**
 * @brief Tag for log system
 */
static const char *tag = "MPU6050";

/**
 * @brief Identifier for accessing calibration data for gyrosope in NVS
 */
static const char *mpac_gyro_nvs = "mpu6050gyro";

static ts_mpu6050_runtime ms_runtime = {
    .b_initialized = false,

    .s_gyro_calib.i16_x = 0,
    .s_gyro_calib.i16_y = 0,
    .s_gyro_calib.i16_z = 0,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Try to initialize sensor
 * @return SENSOR_OK if sensor found.
 */
static e_sensor_error _init(void);

/**
 * @brief Try to deinitialize sensor
 * @return SENSOR_OK if no error
 */
static e_sensor_error _deinit(void);

/**
 * @brief Get raw value from required feature
 * @param e_feature Item from feature list
 * @param[out] ps_data Pointer to memory where data will be stored
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _get_raw(const te_sensor_feature e_feature,
                               ts_sensor_utils_data *ps_data);

/**
 * @brief Convert value from raw to integer for required feature
 *
 * @param s_in_raw Input value in raw format
 * @param[out] ps_out Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _convert_raw_to_int(const ts_sensor_utils_data s_in_raw,
                                          ts_sensor_utils_data *ps_out);

/**
 * @brief Convert value from raw for float for required feature
 *
 * @param s_in_raw Input value in raw format
 * @param[out] puOut Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _convert_raw_to_float(const ts_sensor_utils_data s_in_raw,
                                            ts_sensor_utils_data *ps_out);

// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Tells if given feature need calibration or not
 *
 * Once calibrated, it will always return "calibration not needed"
 *
 * @param e_feature Item from feature list
 * @param[out] pb_need_calibrate Tells if calibration is needed or not
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _need_calibrate(const te_sensor_feature e_feature,
                                      bool *pb_need_calibrate);

/**
 * @brief Calibrate sensor for given feature
 * @param e_feature Item from feature list
 * @param e_level Define calibration precision
 *        Typically higher value gives better results, but it takes more time.
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _calibrate(const te_sensor_feature e_feature,
                                 const ts_sensor_calibration e_level);
// ================| Internal function prototypes: low level |================
/**
 * @brief Load vector data from sensor
 * @param u8_register_address Starting register address
 * @param[out] ps_data Pointer to data structure where result will be written
 * @return Zero if no error
 */
static e_sensor_error _load_xyz(const uint8_t u8_register_address,
                                ts_sensor_utils_data *ps_data);

/**
 * @brief Load calibration data for gyroscope from NVS
 *
 * @param[out] ps_gyro_calib Calibration data will be written here
 * @return SENSOR_OK if no error
 */
static e_sensor_error _gyro_calib_get_from_nvs(ts_sensor_vect *ps_gyro_calib);

/**
 * @brief Save calibration data for gyroscope to NVS
 *
 * @param[in] ps_gyro_calib Calibration data which will be stored in NVS
 * @return SENSOR_OK if no error
 */
static e_sensor_error _gyro_calib_set_to_nvs(ts_sensor_vect *ps_gyro_calib);

// =========================| High level functions |==========================
const ts_sensor_descriptor *mpu6050_get_descriptor(void) {
  // Note - do not care if initialized or not. This should work always

  // Structure describing this sensor driver
  static const ts_sensor_descriptor s_descriptor = {
      .pac_name = "MPU6050",

      .u32_features = (1 << SENSOR_FEATURE_ACCELEROMETER) |
                      (1 << SENSOR_FEATURE_GYROSCOPE) |
                      (1 << SENSOR_FEATURE_TEMPERATURE),
      .i2c =
          {
              .u32_min_speed_bps = 1,
              .u32_max_speed_bps = 400000,
              .u32_recommended_speed_bps = 100000,
          },

      // Fill callbacks
      .cb =
          {
              .pf_init = _init,
              .pf_deinit = _deinit,

              .pf_get_raw = _get_raw,

              .pf_conv_to_int = _convert_raw_to_int,
              .pf_conv_to_float = _convert_raw_to_float,

              .pf_need_calibration = _need_calibrate,
              .pf_calibrate = _calibrate,
          },
  };

  return &s_descriptor;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static e_sensor_error _init(void) {
  if (ms_runtime.b_initialized) {
    // Already initialized
    return SENSOR_OK;
  }

  ms_runtime.b_initialized = false;

  // Expect that I2C bus is already initialized
  const uint8_t au8_address_list[] = MPU6050_I2C_ADDR_LIST;
  const uint8_t au8_who_am_i_reg_values[] = {MPU6050_REG_WHO_I_AM_117_DEFAULT};

  /* Last argument (the "0") is pointer to found register value (if any). We do
   * not need this information, therefore pointer is empty by purpose.
   */
  SENSORS_RET_IF_ERR(sensor_check_who_am_i_list(
      au8_address_list, SENSORS_NUM_OF_ITEMS(au8_address_list),
      MPU6050_REG_WHO_AM_I, au8_who_am_i_reg_values,
      SENSORS_NUM_OF_ITEMS(au8_who_am_i_reg_values), &ms_runtime.u8_address,
      0));

  // Setup sensor
  // =============================| Reset device |==========================
  tu_mpu6050_power_mgmnt_107 u_pwr_mgmnt = {
      .s.DEVICE_RESET = 1,
  };

  tu_mpu6050_sig_path_rst_104 u_sig_path_rst = {
      .s.TEMP_RESET = 1,
      .s.ACCEL_RESET = 1,
      .s.GYRO_RESET = 1,
  };

  SENSORS_LOGD(tag, "Reset IC... (%x)", u_pwr_mgmnt.u8_raw);
  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address,
                                      MPU6050_REG_POWER_MANAMEMENT_1,
                                      u_pwr_mgmnt.u8_raw));

  // While this flag is "1", device is resetting

  while (u_pwr_mgmnt.s.DEVICE_RESET) {
    // Load register value
    SENSORS_RET_IF_ERR(
        sensor_read_reg(ms_runtime.u8_address, MPU6050_REG_POWER_MANAMEMENT_1,
                        &u_pwr_mgmnt.u8_raw, sizeof(u_pwr_mgmnt.u8_raw)));
  }

  // According to the datasheet, 100 ms delay should be applied
  sensor_delay_ms(100);

  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address,
                                      MPU6050_REG_SIGNAL_PATH_RESET,
                                      u_sig_path_rst.u8_raw));

  // According to the datasheet, another 100 ms delay should be applied
  sensor_delay_ms(100);

  // ===========================| Remove sleep flag |=======================
  u_pwr_mgmnt.s.SLEEP = 0;
  SENSORS_LOGD(tag, "Removing sleep flag");
  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address,
                                      MPU6050_REG_POWER_MANAMEMENT_1,
                                      u_pwr_mgmnt.u8_raw));

  // =========================| Load calibration data |=====================
  SENSORS_RET_IF_ERR(_gyro_calib_get_from_nvs(&ms_runtime.s_gyro_calib));

  // ========================| Dummy read sensors data |====================
  // Since after reset all registers are set to 0, including sensor
  // registers, one dummy read is necessary to let internal logic overwrite
  // these values
  SENSORS_LOGD(tag, "First reading from sensors");
  ts_sensor_utils_data s_data;

  // Need to set "initialized" flag in order to be able call functions below
  ms_runtime.b_initialized = true;

  SENSORS_RET_IF_ERR(_get_raw(SENSOR_FEATURE_ACCELEROMETER, &s_data));

  // Wait for first valid reading from sensor. If register value is 0,
  // sensor is not ready and it returns MPU6050_INVALID_TEMP value
  do {
    SENSORS_RET_IF_ERR(_get_raw(SENSOR_FEATURE_TEMPERATURE, &s_data));
  } while (s_data.u_d.i16 == MPU6050_INVALID_TEMP);

  return SENSOR_OK;
}

static e_sensor_error _deinit(void) {
  if (!ms_runtime.b_initialized) {
    // Not initialized - nothing to deinitialize
    return SENSOR_OK;
  }

  // Just for case when debug level will be completely off, avoid compile
  // warning
  (void)tag;

  // The sleep flag should do the job
  tu_mpu6050_power_mgmnt_107 u_pwr_mgmnt = {
      .s.SLEEP = 1,
  };

  e_sensor_error e_err_code =
      sensor_write_reg(ms_runtime.u8_address, MPU6050_REG_POWER_MANAMEMENT_1,
                       u_pwr_mgmnt.u8_raw);

  ms_runtime.b_initialized = false;

  return e_err_code;
}

static e_sensor_error _get_raw(const te_sensor_feature e_feature,
                               ts_sensor_utils_data *ps_data) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  e_sensor_error e_err_code = SENSOR_FAIL;

  // Clean up data container
  memset(ps_data, 0, sizeof(ts_sensor_utils_data));

  switch (e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      e_err_code = _load_xyz(MPU6050_REG_ACCEL_XOUT_H, ps_data);
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      e_err_code = _load_xyz(MPU5050_REG_GYRO_XOUT_H, ps_data);

      // Add correction
      ps_data->u_d.s_vect.i16_x += ms_runtime.s_gyro_calib.i16_x;
      ps_data->u_d.s_vect.i16_y += ms_runtime.s_gyro_calib.i16_y;
      ps_data->u_d.s_vect.i16_z += ms_runtime.s_gyro_calib.i16_z;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      e_err_code = sensor_read_reg(
          ms_runtime.u8_address, MPU6050_REG_TEMP_OUT_H,
          (uint8_t *)&ps_data->u_d.i16, sizeof(ps_data->u_d.i16));

      // When reading, MSByte is read first and stored to lower Byte -> need to
      // swap
      sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.i16);

      ps_data->e_data_type = SENSOR_DTYPE_RAW;
      break;
    default:
      e_err_code = SENSOR_NOT_SUPPORTED;
  }

  // Add information about feature into measured data
  ps_data->e_feature = e_feature;

  return e_err_code;
}

static e_sensor_error _convert_raw_to_int(const ts_sensor_utils_data s_in_raw,
                                          ts_sensor_utils_data *ps_out) {
  int32_t i32_tmp_x, i32_tmp_y, i32_tmp_z;

  switch (s_in_raw.e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      // MPU6050_ACC_RES - this value basically tells what is 1 G value. Program
      // need to return integer value. It would be impractical to return 1 G
      // resolution, so result will be in mG. Need to use 32 bit value to avoid
      // overflow
      i32_tmp_x = (1000 * (int32_t)s_in_raw.u_d.s_vect.i16_x) / MPU6050_ACC_RES;
      i32_tmp_y = (1000 * (int32_t)s_in_raw.u_d.s_vect.i16_y) / MPU6050_ACC_RES;
      i32_tmp_z = (1000 * (int32_t)s_in_raw.u_d.s_vect.i16_z) / MPU6050_ACC_RES;

      ps_out->u_d.s_vect.i16_x = i32_tmp_x;
      ps_out->u_d.s_vect.i16_y = i32_tmp_y;
      ps_out->u_d.s_vect.i16_z = i32_tmp_z;

      ps_out->e_data_type = SENSOR_DTYPE_INT_VECT;
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      // Gyroscope returns degrees per second. Considering gyroscope resolution,
      // it does not make sense work in 1/1000 of degrees, but degrees itself
      // might be inaccurate. So final value is multiplied by 10 to get at least
      // one number after decimal point accuracy
      i32_tmp_x = (10 * (int32_t)s_in_raw.u_d.s_vect.i16_x) / MPU6050_GYRO_RES;
      i32_tmp_y = (10 * (int32_t)s_in_raw.u_d.s_vect.i16_y) / MPU6050_GYRO_RES;
      i32_tmp_z = (10 * (int32_t)s_in_raw.u_d.s_vect.i16_z) / MPU6050_GYRO_RES;

      ps_out->u_d.s_vect.i16_x = i32_tmp_x;
      ps_out->u_d.s_vect.i16_y = i32_tmp_y;
      ps_out->u_d.s_vect.i16_z = i32_tmp_z;

      ps_out->e_data_type = SENSOR_DTYPE_INT_VECT;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      // Returning 10x degrees of celsius -> that is why 10x. Other values are
      // according to the datasheet
      // C = (TEMP_OUT Register Value as a signed quantity)/340 + 36.53

      i32_tmp_x = ((10 * (int32_t)s_in_raw.u_d.i16) / 340) + 370;

      // Clean up other fields
      memset(ps_out, 0, sizeof(ts_sensor_utils_data));

      ps_out->u_d.i16 = i32_tmp_x;

      ps_out->e_data_type = SENSOR_DTYPE_INT;
      break;

    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  // Copy information about feature
  ps_out->e_feature = s_in_raw.e_feature;

  return SENSOR_OK;
}

static e_sensor_error _convert_raw_to_float(const ts_sensor_utils_data s_in_raw,
                                            ts_sensor_utils_data *ps_out) {
  te_sensor_data_type e_expected_in_type;

  switch (s_in_raw.e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      // Return value in mG
      ps_out->u_d.s_vect_float.f_x =
          ((float)s_in_raw.u_d.s_vect.i16_x * 1000) / MPU6050_ACC_RES;
      ps_out->u_d.s_vect_float.f_y =
          ((float)s_in_raw.u_d.s_vect.i16_y * 1000) / MPU6050_ACC_RES;
      ps_out->u_d.s_vect_float.f_z =
          ((float)s_in_raw.u_d.s_vect.i16_z * 1000) / MPU6050_ACC_RES;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      ps_out->u_d.s_vect_float.f_x =
          ((float)s_in_raw.u_d.s_vect.i16_x) / MPU6050_GYRO_RES;
      ps_out->u_d.s_vect_float.f_y =
          ((float)s_in_raw.u_d.s_vect.i16_y) / MPU6050_GYRO_RES;
      ps_out->u_d.s_vect_float.f_z =
          ((float)s_in_raw.u_d.s_vect.i16_z) / MPU6050_GYRO_RES;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      // Clean up other fields
      memset(ps_out, 0, sizeof(ts_sensor_utils_data));

      // According to the datasheet:
      // C = (TEMP_OUT Register Value as a signed quantity)/340 + 36.53
      ps_out->u_d.f = (((float)s_in_raw.u_d.i16) / 340) + 36.53;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT;
      e_expected_in_type = SENSOR_DTYPE_RAW;
      break;

    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  // Copy information about feature
  ps_out->e_feature = s_in_raw.e_feature;

  // If input type does not match, report it. But it does not mean, that
  // conversion could not be done from mathematical point of view
  if (e_expected_in_type != s_in_raw.e_data_type) {
    return SENSOR_INVALID_DATA_TYPE;
  } else {
    return SENSOR_OK;
  }
}
// ===================| Internal functions: middle level |====================
static e_sensor_error _need_calibrate(const te_sensor_feature e_feature,
                                      bool *pb_need_calibrate) {
  *pb_need_calibrate = false;

  switch (e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      break;
    case SENSOR_FEATURE_GYROSCOPE:
      *pb_need_calibrate = true;
      break;
    case SENSOR_FEATURE_TEMPERATURE:
      break;
    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  return SENSOR_OK;
}

static e_sensor_error _calibrate(const te_sensor_feature e_feature,
                                 const ts_sensor_calibration e_level) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // Only gyroscope require calibration. Accelerometer does not need it and
  // temperature sensor as well
  if (e_feature != SENSOR_FEATURE_GYROSCOPE) {
    return SENSOR_NOT_SUPPORTED;
  }

  // Transform level to specific number
  uint16_t u16_num_of_iterations = sensor_get_recommended_num_of_iterations(
      SENSOR_FEATURE_GYROSCOPE, e_level);

  // At least 1 iteration is required
  assert(u16_num_of_iterations);

  // Reset current calibration data
  memset(&ms_runtime.s_gyro_calib, 0, sizeof(ms_runtime.s_gyro_calib));

  SENSORS_RET_IF_ERR(sensor_calibrate_gyroscope(_get_raw, u16_num_of_iterations,
                                                &ms_runtime.s_gyro_calib));

  return _gyro_calib_set_to_nvs(&ms_runtime.s_gyro_calib);
}

// =====================| Internal functions: low level |=====================
static e_sensor_error _load_xyz(const uint8_t u8_register_address,
                                ts_sensor_utils_data *ps_data) {
  e_sensor_error e_err_code = sensor_read_reg(
      ms_runtime.u8_address, u8_register_address,
      (uint8_t *)&ps_data->u_d.s_vect, sizeof(ps_data->u_d.s_vect));

  sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.s_vect.i16_x);
  sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.s_vect.i16_y);
  sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.s_vect.i16_z);

  ps_data->e_data_type = SENSOR_DTYPE_RAW_VECT;

  return e_err_code;
}

static e_sensor_error _gyro_calib_get_from_nvs(ts_sensor_vect *ps_gyro_calib) {
  // As default (if nothing stored in NVS), use zeros - no correction
  ts_sensor_vect s_vect_default;
  memset(&s_vect_default, 0, sizeof(s_vect_default));

  return sensor_nvs_get(mpac_gyro_nvs, ps_gyro_calib, &s_vect_default,
                        sizeof(ts_sensor_vect));
}

static e_sensor_error _gyro_calib_set_to_nvs(ts_sensor_vect *ps_gyro_calib) {
  return sensor_nvs_set(mpac_gyro_nvs, ps_gyro_calib, sizeof(ts_sensor_vect));
}
