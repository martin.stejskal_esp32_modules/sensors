/**
 * @file
 * @author Martin Stejskal
 * @brief HAL for HSCDTD008A
 */
#ifndef __HSCDTD008A_H__
#define __HSCDTD008A_H__
// ===============================| Includes |================================
#include "sensors_common.h"
// ================================| Defines |================================
/**
 * @brief List of I2C address for HSCDTD008A
 *
 * @note Address is fixed. No alternatives available.
 */
#define HSCDTD008A_I2C_ADDR_LIST \
  { 0x0C, 0x0F }

/**
 * @brief Magnetometer resolution (LSB per 1T)
 *
 * 0.15 uT per LSB
 */
#define HSCDTD008A_MAG_RES (6666667)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Returns descriptor for this sensor
 * @return Pointer to driver descriptor
 */
const ts_sensor_descriptor *hscdtd008a_get_descriptor(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __HSCDTD008A_H__
