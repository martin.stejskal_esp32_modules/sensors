/**
 * @file
 * @author Martin Stejskal
 * @brief HAL for HSCDTD008A
 */
// ===============================| Includes |================================
#include "hscdtd008a.h"

#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "sensors_common_private.h"
// ================================| Defines |================================
/**
 * @brief Register address for device
 *
 * @{
 */
#define REG_WHO_AM_I (0x0F)
#define REG_OUTPUT_X_LSB (0x10)
#define REG_STATUS (0x18)
#define REG_CONTROL_1 (0x1B)
#define REG_CONTROL_2 (0x1C)
#define REG_CONTROL_3 (0x1D)
#define REG_CONTROL_4 (0x1E)
#define REG_TEMP (0x31)
/**
 * @}
 */

/**
 * @brief Default values for selected registers
 *
 * @{
 */
#define REG_DEFAULT_WHO_AM_I (0x49)
/**
 * @}
 */
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
// ===============================| Registers |===============================
// Expected Little Endian - LSB is "first" in list
typedef union {
  struct {
    uint8_t ORDY : 1;  // Do not change. Keep default 0
    uint8_t TRDY : 1;  // Do not change. Keep default 0
    uint8_t FFU : 1;   // FIFO full alarm
    uint8_t : 2;
    uint8_t DOR : 1;   // Data overrun detection
    uint8_t DRDY : 1;  // Data ready
    uint8_t : 1;
  } s;
  uint8_t u8_raw;
} tu_status_0x18;

typedef enum {
  R_0x1B_PC_STAND_BY_MODE = 0,
  R_0x1B_PC_ACTIVE_MODE = 1
} te_0x1B_PC;

typedef enum {
  R_0x1B_FS_NORMAL_STATE = 0,
  R_0x1B_FS_FORCE_STATE = 1,  // Default
} te_0x1B_FS;

typedef enum {
  R_0x18_ODR_0_5_Hz = 0,  // 0.5 Hz
  R_0x18_ODR_10_Hz = 1,   // Default
  R_0x18_ODR_20_Hz = 2,
  R_0x18_ODR_100_Hz = 3,
} te_0x18_ODR;

typedef union {
  struct {
    uint8_t : 1;
    te_0x1B_FS FS : 1;  // State control in active mode
    uint8_t : 1;
    te_0x18_ODR ODR : 2;  // Output data rate in normal state
    uint8_t : 2;
    te_0x1B_PC PC : 1;  // Power control
  } s;

  uint8_t u8_raw;
} tu_control_1_0x1B;

typedef union {
  struct {
    uint8_t DOS : 1;  // Do not change. Keep default 0
    uint8_t DTS : 1;  // Do not change. Keep default 0
    uint8_t DRP : 1;  // DRDY signal active level. 0 - low, 1 - high
    uint8_t DEN : 1;  // Data ready function control enable
    uint8_t FF : 1;   // FIFO enable
    uint8_t AOR : 1;  // Choice of method data comparison at FIFO. 0 or, 1 and
    uint8_t FCO : 1;  // Data storage method at FIFO. 0 direct, 1 - comparison
    uint8_t AVG : 1;  // Do not change. Keep default 0
  } s;

  uint8_t u8_raw;
} tu_control_2_0x1C;

typedef union {
  struct {
    uint8_t OCL : 1;  // Start calibrate offset in active mode
    uint8_t TCS : 1;  // Start to measure temperature in active mode
    uint8_t : 2;
    uint8_t STG : 1;  // Self test control enable
    uint8_t : 1;
    uint8_t FRC : 1;   // Start to measure in force state
    uint8_t SRST : 1;  // Software reset
  } s;

  uint8_t u8_raw;
} tu_control_3_0x1D;

typedef enum {
  R_0x1E_RS_14_BIT = 0,  // Default
  R_0x1E_RS_15_BIT = 1,
} te_0x1E_RS;

typedef union {
  struct {
    uint8_t : 3;
    uint8_t AS : 1;  // Do not change. Keep default 0
    te_0x1E_RS RS : 1;
    uint8_t : 1;
    uint8_t MMD : 2;  // Do not change. Keep default 0b10
  } s;

  uint8_t u8_raw;
} tu_control_4_0x1E;

// ==============================| /Registers |===============================
typedef struct {
  // Hard iron offset
  ts_sensor_vect s_offset;

  // Soft iron correction. Multiplied by 1000x (because value is fractional
  // and we're working with integer and not float).
  ts_sensor_vect s_scale;
} ts_mag_runtime_calibration;

typedef struct {
  // Is module initialized?
  bool b_initialized;

  // Calibration factory data for magnetometer
  ts_sensor_vect s_mag_factory_calib;

  ///@brief Calibration data obtained manually
  ///
  /// These data are obtained when calibration function for magnetometer
  /// is called. Then user have to move device around all axes multiple
  /// times to get calibration data. This can not be done by manufacturer
  /// since during soldering MEMS characteristic are changed
  ts_mag_runtime_calibration s_mag_runtime_calib;

  // Device address on the bus
  uint8_t u8_address;
} ts_runtime;
// ===========================| Global variables |============================
/**
 * @brief Tag for log system
 */
static const char *tag = "HSCDTD008A";

/**
 * @brief Key for magnetometer calibration data
 */
static const char *mpac_mag_nvs = "hscdtd008aMag";

/**
 * @brief Runtime variable
 *
 * Calibration data will be loaded during initialization. No need to reset.
 */
static ts_runtime ms_runtime = {
    .b_initialized = false,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Try to initialize sensor
 * @return SENSOR_OK if sensor found.
 */
static e_sensor_error _init(void);

/**
 * @brief Try to deinitialize sensor
 * @return SENSOR_OK if no error
 */
static e_sensor_error _deinit(void);

/**
 * @brief Get raw value from required feature
 * @param e_feature Item from feature list
 * @param[out] ps_data Pointer to memory where data will be stored
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _get_raw(const te_sensor_feature e_feature,
                               ts_sensor_utils_data *ps_data);

/**
 * @brief Convert value from raw to integer for required feature
 *
 * @param s_in_raw Input value in raw format
 * @param[out] ps_out Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _convert_raw_to_int(const ts_sensor_utils_data s_in_raw,
                                          ts_sensor_utils_data *ps_out);

/**
 * @brief Convert value from raw for float for required feature
 *
 * @param s_in_raw Input value in raw format
 * @param[out] puOut Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _convert_raw_to_float(const ts_sensor_utils_data s_in_raw,
                                            ts_sensor_utils_data *ps_out);
// ==============| Internal function prototypes: middle level |===============

/**
 * @brief Tells if given feature need calibration or not
 *
 * Once calibrated, it will always return "calibration not needed"
 *
 * @param e_feature Item from feature list
 * @param[out] pb_need_calibrate Tells if calibration is needed or not
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _need_calibrate(const te_sensor_feature e_feature,
                                      bool *pb_need_calibrate);

/**
 * @brief Calibrate sensor for given feature
 * @param e_feature Item from feature list
 * @param e_level Define calibration precision
 *        Typically higher value gives better results, but it takes more time.
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _calibrate(const te_sensor_feature e_feature,
                                 const ts_sensor_calibration e_level);
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
const ts_sensor_descriptor *hscdtd008a_get_descriptor(void) {
  // Note - do not care if initialized or not. This should work always

  // Structure describing this sensor driver
  static const ts_sensor_descriptor s_descriptor = {
      .pac_name = "HSCDTD008A",

      .u32_features = (1 << SENSOR_FEATURE_TEMPERATURE) |
                      (1 << SENSOR_FEATURE_MAGENETOMETER),
      .i2c =
          {
              .u32_min_speed_bps = 1,
              .u32_max_speed_bps = 400000,
              .u32_recommended_speed_bps = 100000,
          },

      // Fill callbacks
      .cb =
          {
              .pf_init = _init,
              .pf_deinit = _deinit,

              .pf_get_raw = _get_raw,

              .pf_conv_to_int = _convert_raw_to_int,
              .pf_conv_to_float = _convert_raw_to_float,

              .pf_need_calibration = _need_calibrate,
              .pf_calibrate = _calibrate,
          },
  };

  return &s_descriptor;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static e_sensor_error _init(void) {
  if (ms_runtime.b_initialized) {
    // Already initialized
    return SENSOR_OK;
  }

  ms_runtime.b_initialized = false;

  // Expect that I2C bus is already initialized
  const uint8_t au8_acc_gyro_address_list[] = HSCDTD008A_I2C_ADDR_LIST;

  const uint8_t au8_who_am_i_reg_values[] = {REG_DEFAULT_WHO_AM_I};

  /* Last argument (the "0") is pointer to found register value (if any). We do
   * not need this information, therefore pointer is empty by purpose.
   */
  SENSORS_RET_IF_ERR(sensor_check_who_am_i_list(
      au8_acc_gyro_address_list,
      SENSORS_NUM_OF_ITEMS(au8_acc_gyro_address_list), REG_WHO_AM_I,
      au8_who_am_i_reg_values, SENSORS_NUM_OF_ITEMS(au8_who_am_i_reg_values),
      &ms_runtime.u8_address, 0));

  // =============================| Reset device |============================
  // Reset device - preset to known state
  tu_control_3_0x1D u_reg_ctrl_3 = {
      .s.SRST = 1,
  };

  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address, REG_CONTROL_3,
                                      u_reg_ctrl_3.u8_raw));

  // Wait until reset flag is cleared
  while (u_reg_ctrl_3.s.SRST) {
    SENSORS_RET_IF_ERR(sensor_read_reg(ms_runtime.u8_address, REG_CONTROL_3,
                                       &u_reg_ctrl_3.u8_raw,
                                       sizeof(u_reg_ctrl_3.u8_raw)));
  }

  // =============================| Setup sensor |============================
  tu_control_1_0x1B u_reg_ctrl_1 = {
      // Request per measurement
      .s.FS = R_0x1B_FS_FORCE_STATE,

      // Keep default value, although it will have no effect
      .s.ODR = R_0x18_ODR_10_Hz,

      // Wake sensor
      .s.PC = R_0x1B_PC_ACTIVE_MODE,
  };
  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address, REG_CONTROL_1,
                                      u_reg_ctrl_1.u8_raw));

  // Set 15 bit resolution
  tu_control_4_0x1E u_reg_ctrl_4 = {
      .s.MMD = 2,  // According to the datasheet this is must
      .s.RS = R_0x1E_RS_15_BIT,
  };
  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address, REG_CONTROL_4,
                                      u_reg_ctrl_4.u8_raw));

  // Start internal calibration. Clean up reset flag from previous use.
  // Without this step, output from magnetometer is complete mess
  u_reg_ctrl_3.s.SRST = 0;
  u_reg_ctrl_3.s.OCL = 1;
  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address, REG_CONTROL_3,
                                      u_reg_ctrl_3.u8_raw));

  // =========================| Load calibration data |========================

  ts_mag_runtime_calibration s_vect_default = {
      .s_offset.i16_x = 0,
      .s_offset.i16_y = 0,
      .s_offset.i16_z = 0,

      .s_scale.i16_x = 1000,
      .s_scale.i16_y = 1000,
      .s_scale.i16_z = 1000,
  };

  SENSORS_RET_IF_ERR(
      sensor_nvs_get(mpac_mag_nvs, &ms_runtime.s_mag_runtime_calib,
                     &s_vect_default, sizeof(ms_runtime.s_mag_runtime_calib)));

  ms_runtime.b_initialized = true;

  return SENSOR_OK;
}

static e_sensor_error _deinit(void) {
  if (!ms_runtime.b_initialized) {
    // Not initialized - nothing to deinitialize
    return SENSOR_OK;
  }

  // Just for case when debug level will be completely off, avoid compile
  // warning
  (void)tag;

  // Switch back to stand by mode. Do not care about other registers. They
  // will be reset anyways during initialization
  tu_control_1_0x1B u_control_1 = {
      .s.PC = R_0x1B_PC_STAND_BY_MODE,
  };

  e_sensor_error e_err_code = sensor_write_reg(
      ms_runtime.u8_address, REG_CONTROL_1, u_control_1.u8_raw);

  ms_runtime.b_initialized = false;

  return e_err_code;
}

static e_sensor_error _get_raw(const te_sensor_feature e_feature,
                               ts_sensor_utils_data *ps_data) {
  assert(ps_data);

  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  e_sensor_error e_err_code = SENSOR_FAIL;

  // Registers as variables
  tu_status_0x18 u_reg_status = {0};
  tu_control_3_0x1D u_reg_ctrl_3 = {0};

  // Clean up data container
  memset(ps_data, 0, sizeof(ts_sensor_utils_data));

  switch (e_feature) {
    case SENSOR_FEATURE_MAGENETOMETER:
      // Request measurement
      u_reg_ctrl_3.s.FRC = 1;
      SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address, REG_CONTROL_3,
                                          u_reg_ctrl_3.u8_raw));

      // Wait for measurement data
      while (!u_reg_status.s.DRDY) {
        SENSORS_RET_IF_ERR(sensor_read_reg(ms_runtime.u8_address, REG_STATUS,
                                           &u_reg_status.u8_raw,
                                           sizeof(u_reg_status.u8_raw)));
      }

      // Read measured values
      e_err_code = sensor_read_reg(ms_runtime.u8_address, REG_OUTPUT_X_LSB,
                                   (uint8_t *)&ps_data->u_d.s_vect,
                                   sizeof(ps_data->u_d.s_vect));

      ps_data->e_data_type = SENSOR_DTYPE_RAW_VECT;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      // Request measurement
      u_reg_ctrl_3.s.TCS = 1;
      SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_address, REG_CONTROL_3,
                                          u_reg_ctrl_3.u8_raw));

      // Wait for measurement data
      while (!u_reg_status.s.TRDY) {
        SENSORS_RET_IF_ERR(sensor_read_reg(ms_runtime.u8_address, REG_STATUS,
                                           &u_reg_status.u8_raw,
                                           sizeof(u_reg_status.u8_raw)));
      }

      // Temperature is stored as 8 bit value only -> load only 1 Byte
      e_err_code = sensor_read_reg(ms_runtime.u8_address, REG_TEMP,
                                   (uint8_t *)&ps_data->u_d.i16, 1);

      ps_data->e_data_type = SENSOR_DTYPE_RAW;
      break;

    default:
      e_err_code = SENSOR_NOT_SUPPORTED;
  }

  // Add information about feature into measured data
  ps_data->e_feature = e_feature;

  return e_err_code;
}

static e_sensor_error _convert_raw_to_int(const ts_sensor_utils_data s_in_raw,
                                          ts_sensor_utils_data *ps_out) {
  assert(ps_out);

  // Although conversion function is dummy, there is also stored information
  // about data type. So this kind of check can be done at the end of the
  // function
  te_sensor_data_type e_expected_in_type;

  switch (s_in_raw.e_feature) {
    case SENSOR_FEATURE_MAGENETOMETER:
      // Resolution is 0.15 uT per LSB. But integer can not work with fractional
      // values, so we have to work with multiplied values and then divide it.
      // Original equation: output = 0.15u * LSB
      // Used equation: output = (15 * LSB) / 100

      // Of course, we could use following equation as well:
      //   output = (1e6 * LSB) / HSCDTD008A_MAG_RES
      //
      // But that would bring problems with overflow, so let's keep it simple

      // Need to copy input data into output vector, so function below could
      // process it directly to the output variable
      memcpy(&ps_out->u_d.s_vect, &s_in_raw.u_d.s_vect,
             sizeof(ps_out->u_d.s_vect));

      sensor_multiply_and_divide_int_vector(&ps_out->u_d.s_vect, 15, 100);

      ps_out->e_data_type = SENSOR_DTYPE_INT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;
    case SENSOR_FEATURE_TEMPERATURE:
      // Actually temperature is already in degrees. But typically it is
      // returned 10x multiplied value, so at least 1 decimal point can be used.
      // Although this is not the case, let's standardize it
      ps_out->u_d.i16 = s_in_raw.u_d.i16 * 10;

      ps_out->e_data_type = SENSOR_DTYPE_INT;
      e_expected_in_type = SENSOR_DTYPE_RAW;
      break;
    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  // Copy information about feature
  ps_out->e_feature = s_in_raw.e_feature;

  // If input type does not match, report it. But it does not mean, that
  // conversion could not be done from mathematical point of view
  if (e_expected_in_type != s_in_raw.e_data_type) {
    return SENSOR_INVALID_DATA_TYPE;
  } else {
    return SENSOR_OK;
  }
}

static e_sensor_error _convert_raw_to_float(const ts_sensor_utils_data s_in_raw,
                                            ts_sensor_utils_data *ps_out) {
  assert(ps_out);

  // Although conversion function is dummy, there is also stored information
  // about data type. So this kind of check can be done at the end of the
  // function
  te_sensor_data_type e_expected_in_type;

  switch (s_in_raw.e_feature) {
    case SENSOR_FEATURE_MAGENETOMETER:
      ps_out->u_d.s_vect_float.f_x =
          ((float)s_in_raw.u_d.s_vect.i16_x) / HSCDTD008A_MAG_RES;
      ps_out->u_d.s_vect_float.f_y =
          ((float)s_in_raw.u_d.s_vect.i16_y) / HSCDTD008A_MAG_RES;
      ps_out->u_d.s_vect_float.f_z =
          ((float)s_in_raw.u_d.s_vect.i16_z) / HSCDTD008A_MAG_RES;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      // Raw is value in degrees of Celsius
      ps_out->u_d.f = (float)s_in_raw.u_d.i16;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT;
      e_expected_in_type = SENSOR_DTYPE_RAW;
      break;

    default:
      return SENSOR_NOT_SUPPORTED;
  }

  // If input type does not match, report it. But it does not mean, that
  // conversion could not be done from mathematical point of view
  if (e_expected_in_type != s_in_raw.e_data_type) {
    return SENSOR_INVALID_DATA_TYPE;
  } else {
    return SENSOR_OK;
  }
}

// ===================| Internal functions: middle level |====================
static e_sensor_error _need_calibrate(const te_sensor_feature e_feature,
                                      bool *pb_need_calibrate) {
  *pb_need_calibrate = false;

  switch (e_feature) {
    case SENSOR_FEATURE_MAGENETOMETER:
      *pb_need_calibrate = true;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      break;
    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  return SENSOR_OK;
}

static e_sensor_error _calibrate(const te_sensor_feature e_feature,
                                 const ts_sensor_calibration e_level) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // Only magnetometer can be calibrated
  if (e_feature == SENSOR_FEATURE_MAGENETOMETER) {
    // Transform level to specific number
    uint16_t u16_num_of_iterations =
        sensor_get_recommended_num_of_iterations(e_feature, e_level);

    // Clean up runtime variables. The offset can be zero, but scale
    // need to be set to 1x. And since integer can not work with fractional
    // number, it is working with 1000x multiplier
    memset(&ms_runtime.s_mag_runtime_calib.s_offset, 0,
           sizeof(ms_runtime.s_mag_runtime_calib.s_offset));
    ms_runtime.s_mag_runtime_calib.s_scale.i16_x = 1000;
    ms_runtime.s_mag_runtime_calib.s_scale.i16_y = 1000;
    ms_runtime.s_mag_runtime_calib.s_scale.i16_z = 1000;

    // At least 1 iteration is required
    assert(u16_num_of_iterations);

    SENSORS_RET_IF_ERR(
        sensor_calibrate_magnetometer(_get_raw, u16_num_of_iterations,
                                      &ms_runtime.s_mag_runtime_calib.s_offset,
                                      &ms_runtime.s_mag_runtime_calib.s_scale));

    // Store calibration data into NVS
    return sensor_nvs_set(mpac_mag_nvs, &ms_runtime.s_mag_runtime_calib,
                          sizeof(ms_runtime.s_mag_runtime_calib));

  } else {
    return SENSOR_NOT_SUPPORTED;
  }
}
// =====================| Internal functions: low level |=====================
