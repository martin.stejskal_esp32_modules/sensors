/**
 * @file
 * @author Martin Stejskal
 * @brief HAL for MPU9250
 */
// ===============================| Includes |================================
#include "mpu9250.h"

#include <assert.h>
#include <string.h>

#include "math_vector.h"
#include "sensors_common_private.h"
// ================================| Defines |================================
/**
 * @brief Register numbers for accelerometer and gyroscope
 *
 * @{
 */
#define MPU9250_REG_INT_PIN_BYPASS_EN_CFG (55)
#define MPU9250_REG_ACCEL_XOUT_H (59)
#define MPU9250_REG_TEMP_OUT_H (65)
#define MPU9250_REG_GYRO_XOUT_H (67)
#define MPU9250_REG_POWER_MANAMEMENT_1 (107)
#define MPU9250_REG_WHO_AM_I (117)
/**
 * @}
 */

/**
 * @brief Register numbers for magnetometer
 *
 * @{
 */
#define MPU9250_MAG_REG_WIA (0x00)
#define MPU9250_MAG_REG_ST1 (0x02)
#define MPU9250_MAG_REG_HXL (0x03)
#define MPU9250_MAG_REG_CNTL1 (0x0A)
#define MPU9250_MAG_REG_ASAX (0x10)
/**
 * @}
 */

/**
 * @brief Default value for register 107 (Power Management 1)
 */
#define MPU9250_REG_POWER_MANAGEMENT_1_107_DEFAULT (0x40)

/**
 * @brief Default values for register 117 (Who I am)
 *
 * For some reasons, different chip versions using different value. So we need
 * to treat it as a list and check all possible values.
 */
#define MPU9250_REG_WHO_AM_I_117_DEFAULT_LIST \
  { 0x70, 0x71, 0x73 }

/**
 * @brief Some unnatural value when checking if temperature sensor is ready
 *
 * When temperature sensor is initializing, value in register is zero and due
 * to nature of formula, result is non-zero value. So when sensor is simply
 * not ready yet, function return something ridiculous to make it clean, that
 * this is not typical.
 */
#define MPU9250_INVALID_TEMP (-123)

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
// ===============================| Registers |===============================
// ======================| Accelerometer and Gyroscope |======================
// Expected Little Endian - LSB is "first" in list
typedef union {
  struct {
    uint8_t CLKSEL : 3;
    uint8_t TEMP_DIS : 1;
    uint8_t : 1;
    uint8_t CYCLE : 1;
    uint8_t SLEEP : 1;
    uint8_t DEVICE_RESET : 1;
  } s;
  uint8_t u8_raw;
} tu_power_mgmnt_107;

typedef enum {
  R107_CLKSEL_INTERNAL_8MHZ_OSCILLATOR = 0,
  R107_CLKSEL_PLL_WITH_X_AXIS_GYRO_REF = 1,
  R107_CLKSEL_PLL_WITH_Y_AXIS_GYRO_REF = 2,
  R107_CLKSEL_PLL_WITH_Z_AXIS_GYRO_REF = 3,
  R107_CLKSEL_EXT_32768_HZ_REF = 4,
  R107_CLKSEL_EXT_19200_KHZ_REF = 5,
  R107_CLKSEL_RESERVED = 6,
  R107_CLKSEL_STOP_CLK_KEEP_TIMING_GEN_IN_RESET = 7
} te_power_mgmnt_107_clksel;

typedef union {
  struct {
    uint8_t reserved : 1;
    uint8_t BYPASS_EN : 1;
    uint8_t FSYNC_INT_MODE_EN : 1;
    uint8_t ACTL_FSYNC : 1;
    uint8_t INT_ANYRD_2CLEAR : 1;
    uint8_t LATCH_INT_EN : 1;
    uint8_t OPEN : 1;
    uint8_t ACTL;
  } s;
  uint8_t u8raw;
} tu_int_pin_bypass_en_cfg_55;

// =============================| Magnetometer |==============================
typedef union {
  struct {
    uint8_t DRDY : 1;
    uint8_t DOR : 1;
    uint8_t reserved : 6;
  } s;
  uint8_t u8raw;
} tu_mag_status_1_0x02;

typedef union {
  struct {
    uint8_t MODE : 4;
    uint8_t BIT : 1;  // 0: 14 bit output ; 1: 16 bit output
    uint8_t reserved : 3;
  } s;
  uint8_t u8raw;
} tu_mag_control_1_0x0A;

typedef enum {
  R_0x0A_MODE_POWER_DOWN = 0,
  R_0x0A_MODE_SINGLE_MEASUREMENT = 1,
  R_0x0A_MODE_CONTINUOUS_1 = 2,
  R_0x0A_MODE_CONTINUOUS_2 = 6,
  R_0x0A_MODE_EXTERNAL_TRIGGER_MEASUREMENT = 4,
  R_0x0A_MODE_SELF_TEST = 8,
  R_0x0A_MODE_FUSE_ROM_ACCESS = 15
} te_mag_control_1_0x0A_mode;
// ==============================| /Registers |===============================
typedef struct {
  // Hard iron offset
  ts_sensor_vect s_offset;

  // Soft iron correction. Multiplied by 1000x (because value is fractional
  // and we're working with integer and not float).
  ts_sensor_vect s_scale;
} ts_mag_runtime_calibration;

typedef struct {
  // Is module initialized?
  bool b_initialized;

  // I2C address for accelerometer and gyroscope
  uint8_t u8_acc_gyro_address;

  // I2C address for magnetometer (although it is same IC package, address
  // differs
  uint8_t u8_mag_address;

  // Calibration data for gyroscope
  ts_sensor_vect s_gyro_calib;

  // Calibration factory data for magnetometer
  ts_sensor_vect s_mag_factory_calib;

  ///@brief Calibration data obtained manually
  ///
  /// These data are obtained when calibration function for magnetometer
  /// is called. Then user have to move device around all axes multiple
  /// times to get calibration data. This can not be done by manufacturer
  /// since during soldering MEMS characteristic are changed
  ts_mag_runtime_calibration s_mag_runtime_calib;
} ts_mpu9250_runtime;
// ===========================| Global variables |============================
/**
 * @brief Tag for log system
 */
static const char *tag = "MPU9250";

/**
 * @brief Keys for access gyroscope and magnetometer calibration data
 *
 * @{
 */
static const char *mpac_gyro_nvs = "mpu9250gyro";
static const char *mpac_mag_nvs = "mpu9250mag";
/**
 * @}
 */

/**
 * @brief Runtime variable
 *
 * Calibration data will be loaded during initialization. No need to reset.
 */
static ts_mpu9250_runtime ms_runtime = {
    .b_initialized = false,
};
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
/**
 * @brief Try to initialize sensor
 * @return SENSOR_OK if sensor found.
 */
static e_sensor_error _init(void);

/**
 * @brief Try to deinitialize sensor
 * @return SENSOR_OK if no error
 */
static e_sensor_error _deinit(void);

/**
 * @brief Get raw value from required feature
 * @param e_feature Item from feature list
 * @param[out] ps_data Pointer to memory where data will be stored
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _get_raw(const te_sensor_feature e_feature,
                               ts_sensor_utils_data *ps_data);

/**
 * @brief Convert value from raw to integer for required feature
 *
 * @param s_in_raw Input value in raw format
 * @param[out] ps_out Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _convert_raw_to_int(const ts_sensor_utils_data s_in_raw,
                                          ts_sensor_utils_data *ps_out);

/**
 * @brief Convert value from raw for float for required feature
 *
 * @param s_in_raw Input value in raw format
 * @param[out] puOut Output in mg, uT, Pa or degrees, depending on selected
 *             feature
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _convert_raw_to_float(const ts_sensor_utils_data s_in_raw,
                                            ts_sensor_utils_data *ps_out);
// ==============| Internal function prototypes: middle level |===============
/**
 * @brief Tells if given feature need calibration or not
 *
 * Once calibrated, it will always return "calibration not needed"
 *
 * @param e_feature Item from feature list
 * @param[out] pb_need_calibrate Tells if calibration is needed or not
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _need_calibrate(const te_sensor_feature e_feature,
                                      bool *pb_need_calibrate);

/**
 * @brief Calibrate sensor for given feature
 * @param e_feature Item from feature list
 * @param e_level Define calibration precision
 *        Typically higher value gives better results, but it takes more time.
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         supported
 */
static e_sensor_error _calibrate(const te_sensor_feature e_feature,
                                 const ts_sensor_calibration e_level);
// ================| Internal function prototypes: low level |================
/**
 * @brief Load X, Y and Z value for acceleration or gyroscope
 *
 * @param u8_register_address Requested register to read
 * @param[out] ps_data Loaded data will be written here
 * @return SENSOR_OK if no error
 */
static e_sensor_error _load_xyz_acc_gyro(const uint8_t u8_register_address,
                                         ts_sensor_utils_data *ps_data);

/**
 * @brief Load X, Y and Z value for magnetometer
 *
 * @param[out] ps_data Loaded data will be written here
 * @return SENSOR_OK if no error
 */
static e_sensor_error _load_xyz_mag(ts_sensor_utils_data *ps_data);

/**
 * @brief Calibration process for gyroscope
 *
 * @param e_level Calibration level - speed vs accuracy
 * @return SENSOR_OK if no error
 */
static e_sensor_error _calibrate_gyro(const ts_sensor_calibration e_level);

/**
 * @brief Calibration process for magnetometer
 *
 * @param e_level Calibration level - speed vs accuracy
 * @return SENSOR_OK if no error
 */
static e_sensor_error _calibrate_mag(const ts_sensor_calibration e_level);

/**
 * @brief Load calibration data for gyroscope from NVS
 *
 * @param[out] ps_gyro_calib Calibration data will be written here
 * @return SENSOR_OK if no error
 */
static e_sensor_error _gyro_calib_get_from_nvs(ts_sensor_vect *ps_gyro_calib);

/**
 * @brief Save calibration data for gyroscope to NVS
 *
 * @param[in] ps_gyro_calib Calibration data which will be stored in NVS
 * @return SENSOR_OK if no error
 */
static e_sensor_error _gyro_calib_set_to_nvs(ts_sensor_vect *ps_gyro_calib);

/**
 * @brief Load calibration data for magnetometer from NVS
 *
 * @param[out] ps_mag_runtime_calib Calibration data will be written here
 * @return SENSOR_OK if no error
 */
static e_sensor_error _mag_calib_get_from_nvs(
    ts_mag_runtime_calibration *ps_mag_runtime_calib);

/**
 * @brief Save calibration data for magnetometer to NVS
 *
 * @param[in] ps_mag_runtime_calib Calibration data which will be stored in NVS
 * @return SENSOR_OK if no error
 */
static e_sensor_error _mag_calib_set_to_nvs(
    ts_mag_runtime_calibration *ps_mag_runtime_calib);

/**
 * @brief Apply calibration data to raw value
 *
 * @param ps_data Pointer to raw data loaded from sensor itself
 */
static void _mag_apply_calibration_data(ts_sensor_utils_data *ps_data);
// =========================| High level functions |==========================
const ts_sensor_descriptor *mpu9250_get_descriptor(void) {
  // Note - do not care if initialized or not. This should work always

  // Structure describing this sensor driver
  static const ts_sensor_descriptor s_descriptor = {
      .pac_name = "MPU9250",

      .u32_features = (1 << SENSOR_FEATURE_ACCELEROMETER) |
                      (1 << SENSOR_FEATURE_GYROSCOPE) |
                      (1 << SENSOR_FEATURE_TEMPERATURE) |
                      (1 << SENSOR_FEATURE_MAGENETOMETER),
      .i2c =
          {
              .u32_min_speed_bps = 1,
              .u32_max_speed_bps = 400000,
              .u32_recommended_speed_bps = 100000,
          },

      // Fill callbacks
      .cb =
          {
              .pf_init = _init,
              .pf_deinit = _deinit,

              .pf_get_raw = _get_raw,

              .pf_conv_to_int = _convert_raw_to_int,
              .pf_conv_to_float = _convert_raw_to_float,

              .pf_need_calibration = _need_calibrate,
              .pf_calibrate = _calibrate,
          },
  };

  return &s_descriptor;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static e_sensor_error _init(void) {
  if (ms_runtime.b_initialized) {
    // Already initialized
    return SENSOR_OK;
  }

  ms_runtime.b_initialized = false;

  // Expect that I2C bus is already initialized
  const uint8_t au8_acc_gyro_address_list[] = MPU9250_I2C_ADDR_ACC_GYRO_LIST;

  const uint8_t au8_who_am_i_reg_values[] =
      MPU9250_REG_WHO_AM_I_117_DEFAULT_LIST;

  uint8_t u8_who_am_i_reg_value = 0;
  SENSORS_RET_IF_ERR(sensor_check_who_am_i_list(
      au8_acc_gyro_address_list,
      SENSORS_NUM_OF_ITEMS(au8_acc_gyro_address_list), MPU9250_REG_WHO_AM_I,
      au8_who_am_i_reg_values, SENSORS_NUM_OF_ITEMS(au8_who_am_i_reg_values),
      &ms_runtime.u8_acc_gyro_address, &u8_who_am_i_reg_value));

  // Magnetometer have fixed address then
  ms_runtime.u8_mag_address = MPU9250_I2C_ADDR_MAG;

  // =============================| Reset device |==========================
  tu_power_mgmnt_107 u_pwr_mgmnt = {
      .s.DEVICE_RESET = 1,
  };

  SENSORS_LOGD(tag, "Resetting IC....");

  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_acc_gyro_address,
                                      MPU9250_REG_POWER_MANAMEMENT_1,
                                      u_pwr_mgmnt.u8_raw));

  // ====================| Wait for reset - check if zero |=================
  // While this flag is "1", device is resetting
  while (u_pwr_mgmnt.s.DEVICE_RESET) {
    // Load register value
    SENSORS_RET_IF_ERR(sensor_read_reg(
        ms_runtime.u8_acc_gyro_address, MPU9250_REG_POWER_MANAMEMENT_1,
        &u_pwr_mgmnt.u8_raw, sizeof(u_pwr_mgmnt.u8_raw)));
  }

  // According to the datasheet, 100 ms delay should be applied
  sensor_delay_ms(100);

  // ===========================| Remove sleep flag |=======================
  u_pwr_mgmnt.s.SLEEP = 0;
  SENSORS_LOGD(tag, "Removing sleep flag");
  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_acc_gyro_address,
                                      MPU9250_REG_POWER_MANAMEMENT_1,
                                      u_pwr_mgmnt.u8_raw));

  // =====================| Setup route for magnetometer |==================
  tu_int_pin_bypass_en_cfg_55 u_int_pin_bypass_cfg = {
      .s.BYPASS_EN = 1,
  };
  tu_mag_control_1_0x0A u_mag_control = {.s.MODE = R_0x0A_MODE_FUSE_ROM_ACCESS};

  SENSORS_RET_IF_ERR(sensor_write_reg(ms_runtime.u8_acc_gyro_address,
                                      MPU9250_REG_INT_PIN_BYPASS_EN_CFG,
                                      u_int_pin_bypass_cfg.u8raw));
  /* There is exception for some HW revisions. I do not have enough information
   * to confirm this theory, but simply it seems when "who am I" value is 0x70,
   * then magnetometer is not available.
   */
  if (u8_who_am_i_reg_value == 0x70) {
    SENSORS_LOGW(tag,
                 "This HW revision might not have magnetometer! "
                 "Initialization might fail");
    // Just for debugging purpose
    sensor_scan_bus_print_found_devices_priv();
  }

  // ================| Load calibration data for magnetometer |=============
  // Switch to "ROM access" in order to load calibration data
  SENSORS_RET_IF_ERR(sensor_write_reg(
      ms_runtime.u8_mag_address, MPU9250_MAG_REG_CNTL1, u_mag_control.u8raw));

  // There are 3 coefficients (axe X, Y, Z)
  uint8_t au8_coef_raw[3];
  SENSORS_RET_IF_ERR(sensor_read_reg(ms_runtime.u8_mag_address,
                                     MPU9250_MAG_REG_ASAX, au8_coef_raw,
                                     sizeof(au8_coef_raw)));

  // Calculate coefficients. Note that output is fractional value ->
  // multiplied by 1000 so fractional information is kept although integer
  // is still used
  float fTmp = ((((float)au8_coef_raw[0] - 128) * (0.5) / 128) + 1) * 1000;
  ms_runtime.s_mag_factory_calib.i16_x = (int16_t)fTmp;

  fTmp = ((((float)au8_coef_raw[1] - 128) * (0.5) / 128) + 1) * 1000;
  ms_runtime.s_mag_factory_calib.i16_y = (int16_t)fTmp;

  fTmp = ((((float)au8_coef_raw[2] - 128) * (0.5) / 128) + 1) * 1000;
  ms_runtime.s_mag_factory_calib.i16_z = (int16_t)fTmp;

  // Load calibration data (if any)
  SENSORS_RET_IF_ERR(_gyro_calib_get_from_nvs(&ms_runtime.s_gyro_calib));
  SENSORS_RET_IF_ERR(_mag_calib_get_from_nvs(&ms_runtime.s_mag_runtime_calib));

  // Switch mode to "single measurement"
  u_mag_control.s.MODE = R_0x0A_MODE_SINGLE_MEASUREMENT;
  SENSORS_RET_IF_ERR(sensor_write_reg(
      ms_runtime.u8_mag_address, MPU9250_MAG_REG_CNTL1, u_mag_control.u8raw));

  // ========================| Dummy read sensors data |====================
  // Since after reset all registers are set to 0, including sensor
  // registers, one dummy read is necessary to let internal logic overwrite
  // these values

  // Need to set "initialized" flag in order to be able call functions below
  ms_runtime.b_initialized = true;

  ts_sensor_utils_data s_dummy_data;
  const te_sensor_feature ae_features[] = {
      SENSOR_FEATURE_ACCELEROMETER, SENSOR_FEATURE_GYROSCOPE,
      SENSOR_FEATURE_TEMPERATURE, SENSOR_FEATURE_MAGENETOMETER};

  for (uint8_t u8_feature_idx = 0;
       u8_feature_idx < SENSORS_NUM_OF_ITEMS(ae_features); u8_feature_idx++) {
    SENSORS_RET_IF_ERR(_get_raw(ae_features[u8_feature_idx], &s_dummy_data));
  }

  return SENSOR_OK;
}

static e_sensor_error _deinit(void) {
  if (!ms_runtime.b_initialized) {
    // Not initialized - nothing to deinitialize
    return SENSOR_OK;
  }

  // Just for case when debug level will be completely off, avoid compile
  // warning
  (void)tag;

  // The sleep flag should do the job
  tu_power_mgmnt_107 u_pwr_mgmnt = {
      .s.SLEEP = 1,
  };

  e_sensor_error e_err_code =
      sensor_write_reg(ms_runtime.u8_acc_gyro_address,
                       MPU9250_REG_POWER_MANAMEMENT_1, u_pwr_mgmnt.u8_raw);
  ms_runtime.b_initialized = false;

  return e_err_code;
}

static e_sensor_error _get_raw(const te_sensor_feature e_feature,
                               ts_sensor_utils_data *ps_data) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  e_sensor_error e_err_code = SENSOR_FAIL;

  // Clean up data container
  memset(ps_data, 0, sizeof(ts_sensor_utils_data));

  switch (e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      e_err_code = _load_xyz_acc_gyro(MPU9250_REG_ACCEL_XOUT_H, ps_data);
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      e_err_code = _load_xyz_acc_gyro(MPU9250_REG_GYRO_XOUT_H, ps_data);

      // Add correction
      ps_data->u_d.s_vect.i16_x += ms_runtime.s_gyro_calib.i16_x;
      ps_data->u_d.s_vect.i16_y += ms_runtime.s_gyro_calib.i16_y;
      ps_data->u_d.s_vect.i16_z += ms_runtime.s_gyro_calib.i16_z;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      e_err_code = sensor_read_reg(
          ms_runtime.u8_acc_gyro_address, MPU9250_REG_TEMP_OUT_H,
          (uint8_t *)&ps_data->u_d.i16, sizeof(ps_data->u_d.i16));

      // When reading, MSByte is read first and stored to lower Byte -> need to
      // swap
      sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.i16);

      ps_data->e_data_type = SENSOR_DTYPE_RAW;
      break;
    case SENSOR_FEATURE_MAGENETOMETER:
      e_err_code = _load_xyz_mag(ps_data);

      // If data loaded correctly, apply calibration offset
      if (e_err_code == SENSOR_OK) {
        _mag_apply_calibration_data(ps_data);
      }

      break;
    default:
      e_err_code = SENSOR_NOT_SUPPORTED;
  }

  // Add information about feature into measured data
  ps_data->e_feature = e_feature;

  return e_err_code;
}

static e_sensor_error _convert_raw_to_int(const ts_sensor_utils_data s_in_raw,
                                          ts_sensor_utils_data *ps_out) {
  int32_t i32_tmp_x, i32_tmp_y, i32_tmp_z;

  switch (s_in_raw.e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      // MPU6050_ACC_RES - this value basically tells what is 1 G value. Program
      // need to return integer value. It would be impractical to return 1 G
      // resolution, so result will be in mG. Need to use 32 bit value to avoid
      // overflow
      i32_tmp_x = (1000 * (int32_t)s_in_raw.u_d.s_vect.i16_x) / MPU9250_ACC_RES;
      i32_tmp_y = (1000 * (int32_t)s_in_raw.u_d.s_vect.i16_y) / MPU9250_ACC_RES;
      i32_tmp_z = (1000 * (int32_t)s_in_raw.u_d.s_vect.i16_z) / MPU9250_ACC_RES;

      ps_out->u_d.s_vect.i16_x = i32_tmp_x;
      ps_out->u_d.s_vect.i16_y = i32_tmp_y;
      ps_out->u_d.s_vect.i16_z = i32_tmp_z;

      ps_out->e_data_type = SENSOR_DTYPE_INT_VECT;
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      // Gyroscope returns degrees per second. Considering gyroscope resolution,
      // it does not make sense work in 1/1000 of degrees, but degrees itself
      // might be inaccurate. So final value is multiplied by 10 to get at least
      // one number after decimal point accuracy
      i32_tmp_x = (10 * (int32_t)s_in_raw.u_d.s_vect.i16_x) / MPU9250_GYRO_RES;
      i32_tmp_y = (10 * (int32_t)s_in_raw.u_d.s_vect.i16_y) / MPU9250_GYRO_RES;
      i32_tmp_z = (10 * (int32_t)s_in_raw.u_d.s_vect.i16_z) / MPU9250_GYRO_RES;

      ps_out->u_d.s_vect.i16_x = i32_tmp_x;
      ps_out->u_d.s_vect.i16_y = i32_tmp_y;
      ps_out->u_d.s_vect.i16_z = i32_tmp_z;

      ps_out->e_data_type = SENSOR_DTYPE_INT_VECT;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      // Returning 10x degrees of celsius -> that is why 10x. Other constants
      // are taken from Internet, since they are not defined in datasheet or in
      // technical reference (at least what I've). So take it as it is. This
      // works
      i32_tmp_x = ((10 * (int32_t)s_in_raw.u_d.i16) / 321) + 210;

      // Clean up other fields
      memset(ps_out, 0, sizeof(ts_sensor_utils_data));

      ps_out->u_d.i16 = i32_tmp_x;

      ps_out->e_data_type = SENSOR_DTYPE_INT;
      break;

    case SENSOR_FEATURE_MAGENETOMETER:
      // Resolution is 0.6 uT per LSB -> integer can not work with decimal point
      // numbers -> work with multiplied value and as final step, divide.
      // Output should be in uT (practical range) -> 1e6. But that would not fit
      // into 32 bit range. So multiplier will be 100x lower, but also
      // divider
      i32_tmp_x = ((1000000 / 100) * (int32_t)s_in_raw.u_d.s_vect.i16_x) /
                  (MPU9250_MAG_RES / 100);
      i32_tmp_y = ((1000000 / 100) * (int32_t)s_in_raw.u_d.s_vect.i16_y) /
                  (MPU9250_MAG_RES / 100);
      i32_tmp_z = ((1000000 / 100) * (int32_t)s_in_raw.u_d.s_vect.i16_z) /
                  (MPU9250_MAG_RES / 100);

      ps_out->u_d.s_vect.i16_x = i32_tmp_x;
      ps_out->u_d.s_vect.i16_y = i32_tmp_y;
      ps_out->u_d.s_vect.i16_z = i32_tmp_z;

      ps_out->e_data_type = SENSOR_DTYPE_INT_VECT;
      break;

    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  // Copy information about feature
  ps_out->e_feature = s_in_raw.e_feature;

  return SENSOR_OK;
}

static e_sensor_error _convert_raw_to_float(const ts_sensor_utils_data s_in_raw,
                                            ts_sensor_utils_data *ps_out) {
  te_sensor_data_type e_expected_in_type;

  switch (s_in_raw.e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      // Return value in mG
      ps_out->u_d.s_vect_float.f_x =
          ((float)s_in_raw.u_d.s_vect.i16_x * 1000) / MPU9250_ACC_RES;
      ps_out->u_d.s_vect_float.f_y =
          ((float)s_in_raw.u_d.s_vect.i16_y * 1000) / MPU9250_ACC_RES;
      ps_out->u_d.s_vect_float.f_z =
          ((float)s_in_raw.u_d.s_vect.i16_z * 1000) / MPU9250_ACC_RES;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      ps_out->u_d.s_vect_float.f_x =
          ((float)s_in_raw.u_d.s_vect.i16_x) / MPU9250_GYRO_RES;
      ps_out->u_d.s_vect_float.f_y =
          ((float)s_in_raw.u_d.s_vect.i16_y) / MPU9250_GYRO_RES;
      ps_out->u_d.s_vect_float.f_z =
          ((float)s_in_raw.u_d.s_vect.i16_z) / MPU9250_GYRO_RES;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;

    case SENSOR_FEATURE_TEMPERATURE:
      // Clean up other fields
      memset(ps_out, 0, sizeof(ts_sensor_utils_data));

      // Constants are taken from Internet, since they are not defined in
      // datasheet or in technical reference (at least what I've). So take it as
      // it is. This works
      ps_out->u_d.f = (((float)s_in_raw.u_d.i16) / 321) + 21;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT;
      e_expected_in_type = SENSOR_DTYPE_RAW;
      break;

    case SENSOR_FEATURE_MAGENETOMETER:
      ps_out->u_d.s_vect_float.f_x =
          ((float)s_in_raw.u_d.s_vect.i16_x) / MPU9250_MAG_RES;
      ps_out->u_d.s_vect_float.f_y =
          ((float)s_in_raw.u_d.s_vect.i16_y) / MPU9250_MAG_RES;
      ps_out->u_d.s_vect_float.f_z =
          ((float)s_in_raw.u_d.s_vect.i16_z) / MPU9250_MAG_RES;

      ps_out->e_data_type = SENSOR_DTYPE_FLOAT_VECT;
      e_expected_in_type = SENSOR_DTYPE_RAW_VECT;
      break;

    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  // Copy information about feature
  ps_out->e_feature = s_in_raw.e_feature;

  // If input type does not match, report it. But it does not mean, that
  // conversion could not be done from mathematical point of view
  if (e_expected_in_type != s_in_raw.e_data_type) {
    return SENSOR_INVALID_DATA_TYPE;
  } else {
    return SENSOR_OK;
  }
}
// ===================| Internal functions: middle level |====================
static e_sensor_error _need_calibrate(const te_sensor_feature e_feature,
                                      bool *pb_need_calibrate) {
  *pb_need_calibrate = false;

  switch (e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      break;
    case SENSOR_FEATURE_GYROSCOPE:
      *pb_need_calibrate = true;
      break;
    case SENSOR_FEATURE_TEMPERATURE:
      break;
    case SENSOR_FEATURE_MAGENETOMETER:
      *pb_need_calibrate = true;
      break;
    default:
      // Other features are not supported
      return SENSOR_NOT_SUPPORTED;
  }

  return SENSOR_OK;
}

static e_sensor_error _calibrate(const te_sensor_feature e_feature,
                                 const ts_sensor_calibration e_level) {
  if (!ms_runtime.b_initialized) {
    return SENSOR_NOT_INITIALIZED;
  }

  // Only gyroscope and magnetometer can be calibrated
  switch (e_feature) {
    case SENSOR_FEATURE_GYROSCOPE:
      return _calibrate_gyro(e_level);
    case SENSOR_FEATURE_MAGENETOMETER:
      return _calibrate_mag(e_level);
    default:
      return SENSOR_NOT_SUPPORTED;
  }
}
// =====================| Internal functions: low level |=====================

static e_sensor_error _load_xyz_acc_gyro(const uint8_t u8_register_address,
                                         ts_sensor_utils_data *ps_data) {
  e_sensor_error e_err_code = sensor_read_reg(
      ms_runtime.u8_acc_gyro_address, u8_register_address,
      (uint8_t *)&ps_data->u_d.s_vect, sizeof(ps_data->u_d.s_vect));

  sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.s_vect.i16_x);
  sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.s_vect.i16_y);
  sensor_swap_bytes_16_bit((tu_sensor_16_bit *)&ps_data->u_d.s_vect.i16_z);

  ps_data->e_data_type = SENSOR_DTYPE_RAW_VECT;

  return e_err_code;
}

static e_sensor_error _load_xyz_mag(ts_sensor_utils_data *ps_data) {
  // The magnetic sensor require bit different approach

  // Request 14 bit resolution (0) - 0.6uT/LSB. The 16 bit resolution (1) have
  // 15uT/LSB and therefore it is less sensitive. For compass mode it is better
  // use 14 bit resolution
  tu_mag_control_1_0x0A u_ctrl_reg = {.s.MODE = R_0x0A_MODE_SINGLE_MEASUREMENT,
                                      .s.BIT = 0};
  tu_mag_status_1_0x02 u_status_reg = {0};

  // Need to read magnetometer data and "reset" in order to signal
  // magnetometer that data were read. Data are 16 bit long and there are
  // 3 axes (2x3) plus reset register (+1) -> 7
  uint8_t au8_mag_data[7];

  // Request one measurement
  e_sensor_error e_err_code = sensor_write_reg(
      ms_runtime.u8_mag_address, MPU9250_MAG_REG_CNTL1, u_ctrl_reg.u8raw);
  if (e_err_code) {
    return e_err_code;
  }

  // Wait until not read. If read fails, report it immediately
  while (!u_status_reg.s.DRDY) {
    e_err_code = sensor_read_reg(ms_runtime.u8_mag_address, MPU9250_MAG_REG_ST1,
                                 &u_status_reg.u8raw, sizeof(u_status_reg));

    if (e_err_code) {
      return e_err_code;
    }
  }

  // Read data. Order is X, Y, Z. LSB first
  e_err_code = sensor_read_reg(ms_runtime.u8_mag_address, MPU9250_MAG_REG_HXL,
                               au8_mag_data, sizeof(au8_mag_data));
  if (e_err_code) {
    return e_err_code;
  }

  // Fit data into structure
  ps_data->u_d.s_vect.i16_x = (au8_mag_data[1] << 8) | au8_mag_data[0];
  ps_data->u_d.s_vect.i16_y = (au8_mag_data[3] << 8) | au8_mag_data[2];
  ps_data->u_d.s_vect.i16_z = (au8_mag_data[5] << 8) | au8_mag_data[4];

  // Need to tell data type
  ps_data->e_data_type = SENSOR_DTYPE_RAW_VECT;

  // Note: calibration data should be applied by upper layer
  return e_err_code;
}

static e_sensor_error _calibrate_gyro(const ts_sensor_calibration e_level) {
  // Transform level to specific number
  uint16_t u16_num_of_iterations = sensor_get_recommended_num_of_iterations(
      SENSOR_FEATURE_GYROSCOPE, e_level);

  // At least 1 iteration is required
  assert(u16_num_of_iterations);

  // Reset current calibration data
  memset(&ms_runtime.s_gyro_calib, 0, sizeof(ms_runtime.s_gyro_calib));

  SENSORS_RET_IF_ERR(sensor_calibrate_gyroscope(_get_raw, u16_num_of_iterations,
                                                &ms_runtime.s_gyro_calib));

  return _gyro_calib_set_to_nvs(&ms_runtime.s_gyro_calib);
}

static e_sensor_error _calibrate_mag(const ts_sensor_calibration e_level) {
  // Transform level to specific number
  uint16_t u16_num_of_iterations = sensor_get_recommended_num_of_iterations(
      SENSOR_FEATURE_MAGENETOMETER, e_level);

  // Clean up runtime variables. The offset can be zero, but scale
  // need to be set to 1x. And since integer can not work with fractional
  // number, it is working with 1000x multiplier
  memset(&ms_runtime.s_mag_runtime_calib.s_offset, 0,
         sizeof(ms_runtime.s_mag_runtime_calib.s_offset));
  ms_runtime.s_mag_runtime_calib.s_scale.i16_x = 1000;
  ms_runtime.s_mag_runtime_calib.s_scale.i16_y = 1000;
  ms_runtime.s_mag_runtime_calib.s_scale.i16_z = 1000;

  // At least 1 iteration is required
  assert(u16_num_of_iterations);

  SENSORS_RET_IF_ERR(sensor_calibrate_magnetometer(
      _get_raw, u16_num_of_iterations, &ms_runtime.s_mag_runtime_calib.s_offset,
      &ms_runtime.s_mag_runtime_calib.s_scale));

  // Store calibration data into NVS
  return _mag_calib_set_to_nvs(&ms_runtime.s_mag_runtime_calib);
}

inline static e_sensor_error _gyro_calib_get_from_nvs(
    ts_sensor_vect *ps_gyro_calib) {
  // As default (if nothing stored in NVS), use zeros - no correction
  ts_sensor_vect s_vect_default;
  memset(&s_vect_default, 0, sizeof(s_vect_default));

  return sensor_nvs_get(mpac_gyro_nvs, ps_gyro_calib, &s_vect_default,
                        sizeof(ts_sensor_vect));
}

inline static e_sensor_error _gyro_calib_set_to_nvs(
    ts_sensor_vect *ps_gyro_calib) {
  return sensor_nvs_set(mpac_gyro_nvs, ps_gyro_calib, sizeof(ts_sensor_vect));
}

inline static e_sensor_error _mag_calib_get_from_nvs(
    ts_mag_runtime_calibration *ps_mag_runtime_calib) {
  // As default (if nothing stored in NVS). The offset can be zero, but scale
  // need to be set to 1x. And since integer can not work with fractional
  // number, it is working with 1000x multiplier
  ts_mag_runtime_calibration s_vect_default = {
      .s_offset.i16_x = 0,
      .s_offset.i16_y = 0,
      .s_offset.i16_z = 0,

      .s_scale.i16_x = 1000,
      .s_scale.i16_y = 1000,
      .s_scale.i16_z = 1000,
  };

  return sensor_nvs_get(mpac_mag_nvs, ps_mag_runtime_calib, &s_vect_default,
                        sizeof(ts_mag_runtime_calibration));
}

inline static e_sensor_error _mag_calib_set_to_nvs(
    ts_mag_runtime_calibration *ps_mag_runtime_calib) {
  return sensor_nvs_set(mpac_mag_nvs, ps_mag_runtime_calib,
                        sizeof(ts_mag_runtime_calibration));
}

static void _mag_apply_calibration_data(ts_sensor_utils_data *ps_data) {
  // Factory calibration data. Note that calibration data are multiplied by
  // 1000 -> need to divide later on and that is reason why 32 bit temporary
  // register is needed
  int32_t i32tmp;

  assert(ps_data->e_data_type == SENSOR_DTYPE_RAW_VECT);

  // Factory data
  i32tmp = ((int32_t)ps_data->u_d.s_vect.i16_x *
            (int32_t)ms_runtime.s_mag_factory_calib.i16_x) /
           1000;
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  ps_data->u_d.s_vect.i16_x = (int16_t)i32tmp;

  i32tmp = ((int32_t)ps_data->u_d.s_vect.i16_y *
            (int32_t)ms_runtime.s_mag_factory_calib.i16_y) /
           1000;
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  ps_data->u_d.s_vect.i16_y = (int16_t)i32tmp;

  i32tmp = ((int32_t)ps_data->u_d.s_vect.i16_z *
            (int32_t)ms_runtime.s_mag_factory_calib.i16_z) /
           1000;
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  ps_data->u_d.s_vect.i16_z = (int16_t)i32tmp;

  // Scale is 1000x multiplied -> need to divide in final step
  i32tmp = ((int32_t)(ps_data->u_d.s_vect.i16_x -
                      ms_runtime.s_mag_runtime_calib.s_offset.i16_x) *
            (int32_t)ms_runtime.s_mag_runtime_calib.s_scale.i16_x) /
           1000;
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  ps_data->u_d.s_vect.i16_x = (int16_t)i32tmp;

  i32tmp = ((int32_t)(ps_data->u_d.s_vect.i16_y -
                      ms_runtime.s_mag_runtime_calib.s_offset.i16_y) *
            (int32_t)ms_runtime.s_mag_runtime_calib.s_scale.i16_y) /
           1000;
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  ps_data->u_d.s_vect.i16_y = (int16_t)i32tmp;

  i32tmp = ((int32_t)(ps_data->u_d.s_vect.i16_z -
                      ms_runtime.s_mag_runtime_calib.s_offset.i16_z) *
            (int32_t)ms_runtime.s_mag_runtime_calib.s_scale.i16_z) /
           1000;
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  ps_data->u_d.s_vect.i16_z = (int16_t)i32tmp;
}
