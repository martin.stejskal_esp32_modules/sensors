/**
 * @file
 * @author Martin Stejskal
 * @brief Architecture specific header for ESP
 *
 * Original design was tested on ESP32, but in general, it should work for
 * other MCU as well
 */
#ifndef __SENSORS_ARCH_ESP_H__
#define __SENSORS_ARCH_ESP_H__
// ===============================| Includes |================================
// Include ESP LOG if any of the macro below is not defined and therefore
// default would be used
#if !defined(SENSORS_GET_CLK_MS_32BIT) || !defined(SENSORS_LOGE) || \
    !defined(SENSORS_LOGW) || !defined(SENSORS_LOGI) ||             \
    !defined(SENSORS_LOGD) || !defined(SENSORS_LOGV)
#include <esp_log.h>
#endif
// ================================| Defines |================================
/**
 * @brief Log related macros
 *
 * Better to use ESP's log system with colors and timestamps, instead of
 * printf()
 */
#ifndef SENSORS_LOGE
#define SENSORS_LOGE(tag, ...) ESP_LOGE(tag, __VA_ARGS__)
#endif

#ifndef SENSORS_LOGW
#define SENSORS_LOGW(tag, ...) ESP_LOGW(tag, __VA_ARGS__)
#endif

#ifndef SENSORS_LOGI
#define SENSORS_LOGI(tag, ...) ESP_LOGI(tag, __VA_ARGS__)
#endif

#ifndef SENSORS_LOGD
#define SENSORS_LOGD(tag, ...) ESP_LOGD(tag, __VA_ARGS__)
#endif

#ifndef SENSORS_LOGV
#define SENSORS_LOGV(tag, ...) ESP_LOGV(tag, __VA_ARGS__)
#endif

// ==========================| Preprocessor checks |==========================

#endif  // __SENSORS_ARCH_ESP_H__
