/**
 * @file
 * @author Martin Stejskal
 * @brief Architecture specific functions for ESP
 *
 * Original design was tested on ESP32, but in general, it should work for
 * other MCU as well
 */
// ===============================| Includes |================================
// Basically most sensors are I2C based
#include <driver/i2c.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "esp_cfg.h"
#include "nvs_settings.h"
#include "sensors_common.h"
// ================================| Defines |================================
#define I2C_TIMEOUT_MS (100000)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
/**
 * @brief Tag for log system
 */
static const char *tag = "Sensors cmn";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static e_sensor_error _esp_err_to_sensor_err(esp_err_t e_esp_err);
static e_sensor_error _nvs_err_to_sensor_err(te_nvs_sttngs_err e_nvs_err);

// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
e_sensor_error sensor_nvs_get(const char *pac_key, void *p_out_value,
                              const void *p_default_value, size_t i_size) {
  te_nvs_sttngs_err e_nvs_err =
      nvs_settings_get(pac_key, p_out_value, p_default_value, i_size);

  // If there is error, try to log error code for later debug
  if (e_nvs_err) {
    SENSORS_LOGE(tag, "NVS get error: %d", e_nvs_err);
  }
  return _nvs_err_to_sensor_err(e_nvs_err);
}

e_sensor_error sensor_nvs_set(const char *pac_key, const void *p_in_value,
                              size_t i_size) {
  te_nvs_sttngs_err e_nvs_err = nvs_settings_set(pac_key, p_in_value, i_size);

  // If there is error, try to log error code for later debug
  if (e_nvs_err) {
    SENSORS_LOGE(tag, "NVS set error: %d", e_nvs_err);
  }
  return _nvs_err_to_sensor_err(e_nvs_err);
}

e_sensor_error sensor_nvs_del(const char *pac_key) {
  te_nvs_sttngs_err e_nvs_err = nvs_settings_del(pac_key);

  // If there is error, try to log error code for later debug
  if (e_nvs_err) {
    SENSORS_LOGE(tag, "NVS del error: %d", e_nvs_err);
  }
  return _nvs_err_to_sensor_err(e_nvs_err);
}
// ==========================| Low level functions |==========================
e_sensor_error sensor_init_i2c(void) {
  esp_err_t e_err_code = ESP_FAIL;

  i2c_config_t s_conf;
  s_conf.mode = I2C_MODE_MASTER;
  s_conf.sda_io_num = SENSORS_I2C_IO_SDA;
  s_conf.scl_io_num = SENSORS_I2C_IO_SCL;
  s_conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
  s_conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
  s_conf.master.clk_speed = SENSOR_DEFAULT_I2C_SCL_SPEED_HZ;
  s_conf.clk_flags = 0;  // Should be cleaned up
  e_err_code = i2c_param_config(SENSORS_I2C_INTERFACE, &s_conf);

  if (e_err_code) {
    return SENSOR_PERIPHERAL_INIT_FAIL;
  }

  e_err_code =
      i2c_driver_install(SENSORS_I2C_INTERFACE, I2C_MODE_MASTER, 0, 0, 0);

  if (e_err_code) {
    return SENSOR_PERIPHERAL_INIT_FAIL;
  } else {
    return SENSOR_OK;
  }
}

e_sensor_error sensor_deinit_i2c(void) {
  if (i2c_driver_delete(SENSORS_I2C_INTERFACE)) {
    // This is probably more important
    return SENSOR_PERIPHERAL_DEINIT_FAIL;
  } else {
    // Return report from connected sensors
    return SENSOR_OK;
  }
}

e_sensor_error sensor_read_reg(const uint8_t u8_dev_addr,
                               const uint8_t u8_reg_addr, uint8_t *pau8_buffer,
                               const uint8_t u8_num_of_bytes) {
  // Pointer check
  assert(pau8_buffer);
  assert(u8_num_of_bytes);

  i2c_cmd_handle_t h_cmd;

  h_cmd = i2c_cmd_link_create();

  // Just silence compiler, when anything about "verbose" log level is used
  (void)tag;

  SENSORS_LOGV(tag, "I2C start");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_start(h_cmd)));

  SENSORS_LOGV(tag, "I2C write - select device");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(
      i2c_master_write_byte(h_cmd, (u8_dev_addr << 1) | I2C_MASTER_WRITE, 1)));

  SENSORS_LOGV(tag, "I2C write - select register");
  SENSORS_RET_IF_ERR(
      _esp_err_to_sensor_err(i2c_master_write_byte(h_cmd, u8_reg_addr, 1)));

  SENSORS_LOGV(tag, "I2C stop");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_stop(h_cmd)));

  SENSORS_LOGV(tag, "I2C begin");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_cmd_begin(
      SENSORS_I2C_INTERFACE, h_cmd, I2C_TIMEOUT_MS / portTICK_PERIOD_MS)));
  i2c_cmd_link_delete(h_cmd);
  // =================================| /Link |==========================

  // =================================| Link |===========================
  h_cmd = i2c_cmd_link_create();

  SENSORS_LOGV(tag, "I2C start");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_start(h_cmd)));

  SENSORS_LOGV(tag, "I2C read - select device");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(
      i2c_master_write_byte(h_cmd, (u8_dev_addr << 1) | I2C_MASTER_READ, 1)));

  for (uint8_t u8_byte_cnt = 0; u8_byte_cnt < u8_num_of_bytes; u8_byte_cnt++) {
    SENSORS_LOGV(tag, "I2C read registers");

    // If this is last Byte
    if (u8_byte_cnt == (u8_num_of_bytes - 1)) {
      SENSORS_RET_IF_ERR(
          _esp_err_to_sensor_err(i2c_master_read_byte(h_cmd, pau8_buffer, 1)));
    } else {
      SENSORS_RET_IF_ERR(
          _esp_err_to_sensor_err(i2c_master_read_byte(h_cmd, pau8_buffer, 0)));
    }

    pau8_buffer++;
  }

  SENSORS_LOGV(tag, "I2C stop");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_stop(h_cmd)));

  SENSORS_LOGV(tag, "I2C cmd begin");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_cmd_begin(
      SENSORS_I2C_INTERFACE, h_cmd, I2C_TIMEOUT_MS / portTICK_PERIOD_MS)));
  i2c_cmd_link_delete(h_cmd);

  // If program went here, everything is cool
  return SENSOR_OK;
}

e_sensor_error sensor_write_reg(const uint8_t u8_dev_addr,
                                const uint8_t u8_reg_addr,
                                const uint8_t u8_value) {
  i2c_cmd_handle_t h_cmd;

  h_cmd = i2c_cmd_link_create();
  // Start bit (queue)
  SENSORS_LOGV(tag, "I2C master start");
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_start(h_cmd)));

  // Write (queue)
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(
      i2c_master_write_byte(h_cmd, (u8_dev_addr << 1) | I2C_MASTER_WRITE, 1)));
  // Write (queue)
  SENSORS_RET_IF_ERR(
      _esp_err_to_sensor_err(i2c_master_write_byte(h_cmd, u8_reg_addr, 1)));
  // Write (queue)
  SENSORS_RET_IF_ERR(
      _esp_err_to_sensor_err(i2c_master_write_byte(h_cmd, u8_value, 1)));

  // Stop bit
  SENSORS_RET_IF_ERR(_esp_err_to_sensor_err(i2c_master_stop(h_cmd)));

  // Actually send data
  i2c_master_cmd_begin(SENSORS_I2C_INTERFACE, h_cmd,
                       I2C_TIMEOUT_MS / portTICK_PERIOD_MS);
  i2c_cmd_link_delete(h_cmd);

  return SENSOR_OK;
}

bool sensor_is_device_on_bus(const uint8_t u8_dev_addr, bool b_write_mode) {
  i2c_rw_t e_rw;

  if (b_write_mode) {
    e_rw = I2C_MASTER_WRITE;
  } else {
    e_rw = I2C_MASTER_READ;
  }

  i2c_cmd_handle_t h_cmd = i2c_cmd_link_create();

  // Do not check error codes when assembling message. Just care about result
  // when call "cmd begin"
  i2c_master_start(h_cmd);

  // The last argument means: "check for ACK"
  i2c_master_write_byte(h_cmd, (u8_dev_addr << 1) | (uint8_t)e_rw, 1);

  i2c_master_stop(h_cmd);

  esp_err_t e_err_code = i2c_master_cmd_begin(
      SENSORS_I2C_INTERFACE, h_cmd, I2C_TIMEOUT_MS / portTICK_PERIOD_MS);
  i2c_cmd_link_delete(h_cmd);

  if (e_err_code) {
    // Fail - device not found on bus
    return false;
  } else {
    // Success - device found on bus
    return true;
  }
}

inline uint32_t sensor_get_time_ms(void) { return esp_log_timestamp(); }

void sensor_delay_ms(const uint32_t u32_delay_ms) {
  const TickType_t u32_delay_ticks = u32_delay_ms / portTICK_PERIOD_MS;

  if (u32_delay_ticks == 0) {
    // At least 1 tick should be there to make sense to use delay
    vTaskDelay(1);
  } else {
    vTaskDelay(u32_delay_ticks);
  }
}

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static e_sensor_error _esp_err_to_sensor_err(esp_err_t e_esp_err) {
  switch (e_esp_err) {
    case ESP_OK:
      return SENSOR_OK;
    case ESP_ERR_INVALID_ARG:
      return SENSOR_INVALID_PARAM;
    case ESP_ERR_INVALID_STATE:
      return SENSOR_INVALID_STATE;
    case ESP_ERR_TIMEOUT:
      return SENSOR_TIMEOUT;
    default:
      return SENSOR_FAIL;
  }
}

static e_sensor_error _nvs_err_to_sensor_err(te_nvs_sttngs_err e_nvs_err) {
  // The NVS have quite specific error codes, so here it is simplified
  if (e_nvs_err == NVS_STTNGS_OK) {
    return SENSOR_OK;
  } else {
    return SENSOR_FAIL;
  }
}
